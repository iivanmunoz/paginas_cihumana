import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
 // Importar componentes que tienen que ver con el Usuario
 import { ActualizaAdministradorComponent } from './components/actualiza-administrador/actualiza-administrador.component';
 import { LoginComponent } from './components/login/login.component';
 import { RegisterComponent } from './components/register/register.component';
 import { AspirantesComponent } from './components/aspirantes/aspirantes.component';
 import { CursosComponent } from './components/cursos/cursos.component';
 import { EditcursoComponent } from './components/cursos/editcurso.component';
 import { RegcursoComponent } from './components/cursos/regcurso.component';
 import { ClientesComponent } from './components/clientes/clientes.component';
 import { EditClienteComponent } from './components/clientes/editcliente.component';
 import { RegclienteComponent } from './components/clientes/regcliente.component';
 import { NuevosClientesComponent } from './components/nuevos-clientes/nuevos-clientes.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { CategoriasBlogComponent } from './components/categorias/categoriasBlog.component';
import { CategoriasSerComponent } from './components/categorias/categoriasSer.component';

 import { PersonalComponent } from './components/personal/personal.component';
 import { EditpersonalComponent } from './components/personal/editpersonal.component';
 import { RegpersonalComponent } from './components/personal/regpersonal.component';
 import { ServiciosComponent } from './components/servicios/servicios.component';
 import { RegserviciosComponent } from './components/servicios/regservicios.component';
 import { EditserviciosComponent } from './components/servicios/editservicios.component';
 import { BlogComponent } from './components/blog/blog.component';
 import { RegblogComponent } from './components/blog/regblog.component';
 import { EditblogComponent } from './components/blog/editblog.component';
 
const appRoutes: Routes = [
 	{ path: 'login', component: LoginComponent },
 	{ path: 'register', component: RegisterComponent },
 	{ path: '', component: ActualizaAdministradorComponent},
 	{ path: 'aspirantes', component: AspirantesComponent},
 	{ path: 'cursos', component: CursosComponent },
 	{ path: 'regcur', component: RegcursoComponent },
 	{ path: 'editcur/:idCurso', component: EditcursoComponent },
 	{path: 'clientes', component:ClientesComponent},
 	{ path: 'editcliente/:idCliente', component: EditClienteComponent },
 	{ path: 'regcliente', component: RegclienteComponent },
 	{ path: 'nuevosclientes', component: NuevosClientesComponent},
 	{ path: 'categorias', component: CategoriasComponent},
 	{ path: 'categorias/blog', component: CategoriasBlogComponent},
 	{ path: 'categorias/servicios', component: CategoriasSerComponent},
 	
 	{ path: 'personal', component: PersonalComponent },
 	{ path: 'editper/:idPersonal', component: EditpersonalComponent},	
 	{ path: 'regper', component: RegpersonalComponent },
 	{ path: 'serv', component: ServiciosComponent },
 	{ path: 'regserv', component: RegserviciosComponent },
 	{ path: 'editserv/:idServicio', component: EditserviciosComponent },
 	{ path: 'blog', component: BlogComponent },
 	{ path: 'regblog', component: RegblogComponent },
 	{ path: 'editblog/:idNoticiablog', component: EditblogComponent }

 ];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);