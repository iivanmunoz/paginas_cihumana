import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CursoService } from '../../services/curso.service';
import { CursoModel } from '../../models/curso.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from'sweetalert2';

@Component({
  selector: 'app-regcurso',
  templateUrl: './regcurso.component.html',
  styleUrls: ['./regcurso.component.css'],
  providers:[CursoService]
})
export class RegcursoComponent implements OnInit {

  	public curso:CursoModel;
  	public url:string;
  	public filesToUpload: Array<File>;
	public alertRegister;
	public token;

	@Output() aparecer = new EventEmitter;

	constructor(
		private _cursoService:CursoService,
		private _route: ActivatedRoute,
		private _router: Router) {
		this.url = GLOBAL.url;
		this.curso = new CursoModel('','','','','','','','');
	}

	ngOnInit() {

	}

	getCurso() {
  	this._route.params.forEach((params: Params) =>{
  		let idCurso = params['idCurso'];
  		this._cursoService.verCurso(idCurso).subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.curso = response.curso,
					console.log(response.curso);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
  	});
  }
	
	// onSubmitRegister(){
	// 	this._cursoService.crea(this.curso).subscribe(
	// 		response=>{
	// 			let curso = response.idCurso;
	// 			this.curso = curso;
	// 			if (!curso) {
	// 				this.alertRegister = 'Error al registrarse';
	// 			}else{
	// 				swal("Exito", response.message, "success");
	// 				this._router.navigate(['/cursos']);
	// 			}
	// 		},error=>{
	// 			var errorMensaje = <any>error;
	// 			if (errorMensaje != null) {
	// 				var body = JSON.parse(error._body);
	// 				this.alertRegister = body.message;
	// 				console.log(error)
	// 			}
	// 		}
	// 		);
	// }

	onSubmitRegister(){
			this._cursoService.crea(this.curso).subscribe(
				response=>{
					console.log(response)
					this.makeFileRquest(this.url+'subeImagenCurso/'+response.idCurso[0],
						[], this.filesToUpload).then(
						(result:any)=>{
							swal("Exito", response.message, "success");
						}
						).catch(error=>{
							console.log(`${error}`);
						})
					},
					error=>{
						var errorMensaje = <any>error;
						if (errorMensaje != null) {
							var body = JSON.parse(error._body);
							console.log(error)
						}
					}
					)
		}

		// Imagen

		fileChangeEvent(fileInput:any){
			this.filesToUpload = <Array<File>>fileInput.target.files;
		}

		makeFileRquest(url:string,params:Array<string>, files:Array<File>){
			let token = this.token;
			return new Promise(function(resolve,reject){
				var formData:any = new FormData();
				var xhr = new XMLHttpRequest();
				for (var i = 0; i < files.length; i++) {
					formData.append('image',files[i],files[i].name)
				}
				xhr.onreadystatechange = function(){
					if (xhr.readyState == 4) {
						if (xhr.status == 200) {
							resolve(JSON.parse(xhr.response));	
						}else{
							reject(xhr.response);
						}
					}
				}
				xhr.open('POST',url,true);
				xhr.setRequestHeader('Authorization',token);
				xhr.send(formData);
			})
		}

}
