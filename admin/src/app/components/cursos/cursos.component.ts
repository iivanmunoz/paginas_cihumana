import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { CursoService } from '../../services/curso.service';
import { CursoModel } from '../../models/curso.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';

export interface DialogData {
  idCurso: string;
  nombreCurso:string,
  descripcionCurso:string,
  aprendizajeCurso:string,
  objetivoCurso:string,
  duracionCurso:string,
  urlCurso:string,
  statusCurso:string,
}

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css'],
  providers:[CursoService]
})
export class CursosComponent implements OnInit {

	public titulo:string;
	public curso:CursoModel;
	public identity;
	public token;
	public alertRegister;
	public url:string;
	public next_page;
	public prev_page;
	public idCurso;
  	public nombreCurso;
  	public descripcionCurso;
  	public aprendizajeCurso;
  	public objetivoCurso;
  	public duracionCurso;
  	public urlCurso;
  	public statusCurso;

	@Output() aparecer = new EventEmitter;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		public _cursoService:CursoService,
		public dialog: MatDialog
		) {
		this.titulo = 'Cursos';
		this.url = GLOBAL.url;
		this.next_page = 1;
		this.prev_page =1;
		
	}

  ngOnInit() {

  	this.getCursos();
  	
  }

  getCurso() {
  	this._route.params.forEach((params: Params) =>{
  		let idCurso = params['idCurso'];
  		this._cursoService.verCurso(idCurso).subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.curso = response.curso,
					console.log(response.curso);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
  	});
  }

  getCursos() {
  	this._route.params.forEach((params:Params) =>{
  		let page = +params['page'];
  		if (!page) {
  			page = 1;
  		}else{
  			this.next_page = page + 1;
  			this.prev_page = page - 1;

  			if (this.prev_page ==0) {
  				this.prev_page = 1;
  			}
  		}

  		this._cursoService.verCursos(page).subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.curso = response.curso,
					console.log(response.curso);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
  	});
  }

  public confirmado;

  elimConf(idCurso){
  	swal({
  		title: '¿Seguro?',
  		text: "Se desactivará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, desactiva el curso.'
	}).then((result) => {
		if (result.value) {
		  this._cursoService.dabaja(idCurso).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Desactivado',
					  'Se ha desactivado el curso.',
					  'success'
					);
					this.getCursos();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

  regreConf(idCurso){
  	swal({
  		title: '¿Seguro?',
  		text: "Se activará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, activa el curso.'
	}).then((result) => {
		if (result.value) {
		  this._cursoService.daalta(idCurso).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Activado',
					  'Se ha activado el curso.',
					  'success'
					);
					this.getCursos();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }


  openDialog(idCurso, nombreCurso, descripcionCurso, aprendizajeCurso, objetivoCurso, duracionCurso, urlCurso): void {
    this.idCurso = idCurso;
    this.nombreCurso = nombreCurso;
    this.descripcionCurso = descripcionCurso;
    this.aprendizajeCurso = aprendizajeCurso;
    this.objetivoCurso = objetivoCurso;
    this.duracionCurso = duracionCurso;
    this.urlCurso = urlCurso;
    const dialogRef = this.dialog.open(DialogCurso, {
      width: '250px',
      data: {idCurso: this.idCurso,
      	nombreCurso: this.nombreCurso,
      	descripcionCurso: this.descripcionCurso,
      	aprendizajeCurso: this.aprendizajeCurso,
      	objetivoCurso: this.objetivoCurso,
      	duracionCurso: this.duracionCurso,
      	urlCurso: this.urlCurso
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

}


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
  styleUrls: ['./cursos.component.css'],
  providers:[CursoService]
})

export class DialogCurso {

  constructor(
  	public _cursoService:CursoService,
    public dialogRef: MatDialogRef<DialogCurso>,
    @Inject(MAT_DIALOG_DATA) public curso: DialogData) {}


  onNoClick(): void {
    this.dialogRef.close();
  }

  onEditClick() {
  	swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
		  	this._cursoService.actualiza(this.curso).subscribe(
					response=>{
						if (!response) {
							swal("Error", "Hubo un problema.", "error")
						}else{
							swal(
							  'Modificado',
							  'Se ha modificado el curso.',
							  'success'
							);
							this.dialogRef.close();
						}
					},error=>{
						var errorMensaje = <any>error;
						if (errorMensaje != null) {
							var body = JSON.parse(error._body);
							console.log(error)
						}
					}
				);
		  }
	  });

	}


}


