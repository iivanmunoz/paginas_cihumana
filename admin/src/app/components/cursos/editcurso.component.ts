import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CursoService } from '../../services/curso.service';
import { CursoModel } from '../../models/curso.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from'sweetalert2';

@Component({
  selector: 'app-editcurso',
  templateUrl: './editcurso.component.html',
  styleUrls: ['./editcurso.component.css'],
  providers:[CursoService]
})
export class EditcursoComponent implements OnInit {

  public curso:CursoModel;
  public alertRegister;
  public url:string;
  public dir;
  public filesToUpload: Array<File>;
  public token;

	constructor(
		private _cursoService:CursoService,
		private _route: ActivatedRoute,
		private _router: Router) {
		this.url = GLOBAL.url;
		this.dir = GLOBAL.url;
		this.curso = new CursoModel ('','','','','','','','');
	}

  ngOnInit() {
  	this.getCurso();
  }

  onSubmitRegister(){
		swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
		  	this._cursoService.actualiza(this.curso).subscribe(
					response=>{
						if (!response) {
							swal("Error", "Hubo un problema.", "error")
						}else{
							this.makeFileRquest(this.url+'subeImagenCurso/'+this.curso.idCurso,
							[], this.filesToUpload).then(
							(result:any)=>{
								this.curso.imagenCurso = result.image.imagenCurso
							}
							).catch(error=>{
								console.log(`${error}`);
							})
							swal(
							  'Modificado',
							  'Se ha modificado el curso.',
							  'success'
							);
						}
					},error=>{
						var errorMensaje = <any>error;
						if (errorMensaje != null) {
							var body = JSON.parse(error._body);
							console.log(error)
						}
					}
				);
		  }
	  });

	}


  public getCurso() {
  	this._route.params.forEach((params: Params) =>{
  		let idCurso = params['idCurso'];
  		this._cursoService.verCurso(idCurso).subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.curso = response.curso[0],
					console.log(this.curso);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
  	});
  }

  fileChangeEvent(fileInput:any){
  	this.filesToUpload = <Array<File>>fileInput.target.files;
  }

  makeFileRquest(url:string,params:Array<string>, files:Array<File>){
		let token = this.token;
		return new Promise(function(resolve,reject){
			var formData:any = new FormData();
			var xhr = new XMLHttpRequest();
			for (var i = 0; i < files.length; i++) {
				formData.append('image',files[i],files[i].name)
			}
			xhr.onreadystatechange = function(){
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
						resolve(JSON.parse(xhr.response));	
					}else{
						reject(xhr.response);
					}
				}
			}
			xhr.open('POST',url,true);
			xhr.setRequestHeader('Authorization',token);
			xhr.send(formData);
		})
	}

}
