import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import * as $ from 'jquery';
import { OnInit, Output, EventEmitter } from '@angular/core';
import { AdministradorService } from '../../services/administrador.service';
import { AdministradorModel } from '../../models/administrador.model';
import { GLOBAL } from '../../services/global';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css'],
	// providers:[AdministradorService]
})

// export class NavbarComponent implements OnInit {
// 	public identity;
// 	public token;
// 	public administrador:AdministradorModel;
// 	public url:string;

// 	@Output() cerrarsesion = new EventEmitter();

// 	constructor(private _administradorService:AdministradorService) {
//   	// LocalStorage
//   	this.identity = this._administradorService.getIdentity();
//   	this.token = this._administradorService.getToken();

//   	this.administrador = this.identity;
//   	this.url = GLOBAL.url;
//   }

//   public logOut(){
//   	let nada = null;
//   	this.cerrarsesion.emit(nada);
//   }

//   ngOnInit() {
//   }

// }
export class NavbarComponent implements OnDestroy {
  mobileQuery: MediaQueryList;
  public side;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private _administradorService:AdministradorService) {
    this.identity = this._administradorService.getIdentity();
    this.token = this._administradorService.getToken();

    this.administrador = this.identity;
    this.url = GLOBAL.url;
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
    public identity;
  public token;
  public administrador:AdministradorModel;
  public url:string;

  @Output() cerrarsesion = new EventEmitter();

  public logOut(){
    let nada = null;
    this.cerrarsesion.emit(nada);
  }

  

  
}

