import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { CategoriasBlogService } from '../../services/categoriaBlog.service';
import { CategoriasBlogModel } from '../../models/categoriasBlog.model';
import { CategoriasServService } from '../../services/categoriaSer.service';
import { CategoriasSerModel } from '../../models/categoriasSer.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css'],
  providers: [CategoriasBlogService,CategoriasServService]
})
export class CategoriasComponent implements OnInit {

	public titulo:string;
	public categoriaBlog:CategoriasBlogModel;
	public categoriaSer:CategoriasSerModel;
	public identity;
	public token;
	public alertRegister;
	public url:string;

   constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		public _categoriasblogService:CategoriasBlogService,
		public _categoriasSerService:CategoriasServService,
		public dialog: MatDialog
		) {
		this.titulo = 'Cursos';
		this.url = GLOBAL.url;
		
	}

  ngOnInit() {
  	this.getCategoriasBlog();
  	this.getCategoriasSer();
  }

  getCategoriasBlog() {
  	this._route.params.forEach((params:Params) =>{
  		this._categoriasblogService.verCategoriasBlog().subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.categoriaBlog = response.CatBlog,
					console.log(response.categoriaBlog);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
  	});
  }

  getCategoriasSer() {
  	this._route.params.forEach((params:Params) =>{
  		this._categoriasSerService.verCategoriasSer().subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.categoriaSer = response.CatServ,
					console.log(response.categoriaSer);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
  	});
  }

  elimCategoriablog(idCategoriablog){
  	swal({
  		title: '¿Seguro?',
  		text: "Se desactivará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, desactiva la categoria.'
	}).then((result) => {
		if (result.value) {
		  this._categoriasblogService.dabajaCategoriaBlog(idCategoriablog).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Desactivado',
					  'Se ha desactivado la categoria.',
					  'success'
					);
					this.getCategoriasBlog();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

  regreCategoriablog(idCategoriablog){
  	swal({
  		title: '¿Seguro?',
  		text: "Se activará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, activa la categoria.'
	}).then((result) => {
		if (result.value) {
		  this._categoriasblogService.daaltaCategoriaBlog(idCategoriablog).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Activado',
					  'Se ha activado la categoria.',
					  'success'
					);
					this.getCategoriasBlog();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

  elimCategoriaser(idCategoriaservicio_Servicio){
  	swal({
  		title: '¿Seguro?',
  		text: "Se desactivará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, desactiva la categoria.'
	}).then((result) => {
		if (result.value) {
		  this._categoriasSerService.dabajaCategoriaSer(idCategoriaservicio_Servicio).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Desactivado',
					  'Se ha desactivado la categoria.',
					  'success'
					);
					this.getCategoriasBlog();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

  regreCategoriaser(idCategoriaservicio_Servicio){
  	swal({
  		title: '¿Seguro?',
  		text: "Se activará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, activa la categoria.'
	}).then((result) => {
		if (result.value) {
		  this._categoriasSerService.daaltaCategoriaSer(idCategoriaservicio_Servicio).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Activado',
					  'Se ha activado la categoria.',
					  'success'
					);
					this.getCategoriasBlog();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

}
