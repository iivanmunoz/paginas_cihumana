import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { CategoriasBlogService } from '../../services/categoriaBlog.service';
import { CategoriasBlogModel } from '../../models/categoriasBlog.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';

export interface DialogData {
  idCategoriablog: string;
  nombreCategoriablog: string;
  statusCategoriaBlog: string;
}

@Component({
  selector: 'app-categoriasblog',
  templateUrl: './categoriasBlog.component.html',
  styleUrls: ['./categorias.component.css'],
  providers: [CategoriasBlogService,]
})
export class CategoriasBlogComponent implements OnInit {

	public titulo:string;
	public categoriablog:CategoriasBlogModel;
	public identity;
	public token;
	public alertRegister;
	public url:string;
	public idCategoriablog;
  public nombreCategoriablog;
  public statusCategoriaBlog;

   constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		public _categoriasblogService:CategoriasBlogService,
		public dialog: MatDialog
		) {
		this.titulo = 'Cursos';
		this.url = GLOBAL.url;
		
	}

	public opencategoriaBlog(idCategoriablog,nombreCategoriablog, statusCategoriaBlog):void{
    this.idCategoriablog = idCategoriablog;
    this.nombreCategoriablog = nombreCategoriablog;
    this.statusCategoriaBlog = statusCategoriaBlog;
    let dialogRef = this.dialog.open(DialogCategoriaBlog, {
      width: '400px',
      data:{
        idCategoriablog:this.idCategoriablog,
        nombreCategoriablog:this.nombreCategoriablog,
        statusCategoriaBlog:this.statusCategoriaBlog
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  public openregcategoriaBlog(idCategoriablog,nombreCategoriablog, statusCategoriaBlog):void{
    this.idCategoriablog = idCategoriablog;
    this.nombreCategoriablog = nombreCategoriablog;
    this.statusCategoriaBlog = statusCategoriaBlog;
    let dialogRef = this.dialog.open(DialogregistroCategoriaBlog, {
      width: '400px',
      data:{
        idCategoriablog:this.idCategoriablog,
        nombreCategoriablog:this.nombreCategoriablog,
        statusCategoriaBlog:this.statusCategoriaBlog
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  ngOnInit() {
  	this.getCategoriasBlog();
  }

  

  getCategoriasBlog() {
  	this._route.params.forEach((params:Params) =>{
  		this._categoriasblogService.verCategoriasBlog().subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.categoriablog = response.CatBlog,
					console.log(response.categoriablog);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
  	});
  }

  elimCategoriablog(idCategoriablog){
  	swal({
  		title: '¿Seguro?',
  		text: "Se desactivará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, desactiva la categoria.'
	}).then((result) => {
		if (result.value) {
		  this._categoriasblogService.dabajaCategoriaBlog(idCategoriablog).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Desactivado',
					  'Se ha desactivado la categoria.',
					  'success'
					);
					this.getCategoriasBlog();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

  regreCategoriablog(idCategoriablog){
  	swal({
  		title: '¿Seguro?',
  		text: "Se activará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, activa la categoria.'
	}).then((result) => {
		if (result.value) {
		  this._categoriasblogService.daaltaCategoriaBlog(idCategoriablog).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Activado',
					  'Se ha activado la categoria.',
					  'success'
					);
					this.getCategoriasBlog();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

}
  @Component({
	  selector: 'app-categoriasBlog-dialog',
	  templateUrl: './popupcategoriasblog.html',
	  styleUrls: ['./categorias.component.css'],
	  providers: [CategoriasBlogService]
	})
	export class DialogCategoriaBlog {

	  constructor(
	  	public _categoriasblogService:CategoriasBlogService,
	    public dialogRef: MatDialogRef<DialogCategoriaBlog>,
	    @Inject(MAT_DIALOG_DATA) public categoriablog: DialogData) {}


	  onNoClick(): void {
	    this.dialogRef.close();
	  }

	  onEditClick() {
  	swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
			console.log(this.categoriablog.idCategoriablog);
		  	this._categoriasblogService.actualizaCategoriaBlog(this.categoriablog).subscribe(
					response=>{
						if (!response) {
							swal("Error", "Hubo un problema.", "error")
						}else{

							swal(
							  'Modificado',
							  'Se ha modificado el curso.',
							  'success'
							);
							this.dialogRef.close();
						}
					},error=>{
					var errorMensaje = <any>error;
					if (errorMensaje != null) {
						var body = JSON.parse(error._body);
						console.log(error)
						}
					}
				);
		  }
	  });

	}

	  

}

@Component({
	  selector: 'app-registrocategoriasBlog-dialog',
	  templateUrl: './popupregcategoriasblog.html',
	  styleUrls: ['./categorias.component.css'],
	  providers: [CategoriasBlogService]
	})
	export class DialogregistroCategoriaBlog {

	  constructor(
	  	public _categoriasblogService:CategoriasBlogService,
	    public dialogRef: MatDialogRef<DialogregistroCategoriaBlog>,
	    @Inject(MAT_DIALOG_DATA) public categoriablog: DialogData) {}


	  onNoClick(): void {
	    this.dialogRef.close();
	  }

	  onRegisterClick(){
		this._categoriasblogService.añadirCategoriaBlog(this.categoriablog).subscribe(
			response=>{
				let categoriablog = response.idCategoriablog;
				this.categoriablog = categoriablog;
				if (!categoriablog) {
					// this.alertRegister = 'Error al registrarse';
				}else{
					swal("Exito", response.message, "success");
					this.dialogRef.close();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					// this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
	} 

	  

}
