import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { CategoriasServService } from '../../services/categoriaSer.service';
import { CategoriasSerModel } from '../../models/categoriasSer.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';

export interface DialogData {
  idCategoriaservicio_Servicio: string;
  nombreCategoriaservicio: string;
  statusCategoriaservicio: string;
}

@Component({
  selector: 'app-categoriaserv',
  templateUrl: './categoriasSer.component.html',
  styleUrls: ['./categorias.component.css'],
  providers: [CategoriasServService]
})
export class CategoriasSerComponent implements OnInit {

	public titulo:string;
	public categoriaSer:CategoriasSerModel;
	public identity;
	public token;
	public alertRegister;
	public url:string;
	public idCategoriaservicio_Servicio;
	public nombreCategoriaservicio;
	public statusCategoriaservicio;

   constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		public _categoriasSerService:CategoriasServService,
		public dialog: MatDialog
		) {
		this.titulo = 'Cursos';
		this.url = GLOBAL.url;
		
	}

	public opencategoriaSer(idCategoriaservicio_Servicio,nombreCategoriaservicio, statusCategoriaservicio):void{
    this.idCategoriaservicio_Servicio = idCategoriaservicio_Servicio;
    this.nombreCategoriaservicio = nombreCategoriaservicio;
    this.statusCategoriaservicio = statusCategoriaservicio;
    let dialogRef = this.dialog.open(DialogCategoriaSer, {
      width: '400px',
      data:{
        idCategoriaservicio_Servicio:this.idCategoriaservicio_Servicio,
        nombreCategoriaservicio:this.nombreCategoriaservicio,
        statusCategoriaservicio:this.statusCategoriaservicio
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  public openregcategoriaSer(idCategoriaservicio_Servicio,nombreCategoriaservicio, statusCategoriaservicio):void{
    this.idCategoriaservicio_Servicio = idCategoriaservicio_Servicio;
    this.nombreCategoriaservicio = nombreCategoriaservicio;
    this.statusCategoriaservicio = statusCategoriaservicio;
    let dialogRef = this.dialog.open(DialogregistroCategoriaSer, {
      width: '400px',
      data:{
        idCategoriaservicio_Servicio:this.idCategoriaservicio_Servicio,
        nombreCategoriaservicio:this.nombreCategoriaservicio,
        statusCategoriaservicio:this.statusCategoriaservicio
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  ngOnInit() {
  	this.getCategoriasSer();
  }

 
  getCategoriasSer() {
  	this._route.params.forEach((params:Params) =>{
  		this._categoriasSerService.verCategoriasSer().subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.categoriaSer = response.CatServ,
					console.log(this.categoriaSer);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
  	});
  }


  elimCategoriaser(idCategoriaservicio_Servicio){
  	swal({
  		title: '¿Seguro?',
  		text: "Se desactivará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, desactiva la categoria.'
	}).then((result) => {
		if (result.value) {
		  this._categoriasSerService.dabajaCategoriaSer(idCategoriaservicio_Servicio).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Desactivado',
					  'Se ha desactivado la categoria.',
					  'success'
					);
					this.getCategoriasSer();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

  regreCategoriaser(idCategoriaservicio_Servicio){
  	swal({
  		title: '¿Seguro?',
  		text: "Se activará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, activa la categoria.'
	}).then((result) => {
		if (result.value) {
		  this._categoriasSerService.daaltaCategoriaSer(idCategoriaservicio_Servicio).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Activado',
					  'Se ha activado la categoria.',
					  'success'
					);
					this.getCategoriasSer();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }
}

 @Component({
	  selector: 'app-categoriasSer-dialog',
	  templateUrl: './popupcategoriasser.html',
	  styleUrls: ['./categorias.component.css'],
	  providers: [CategoriasServService]
	})
	export class DialogCategoriaSer {

	  constructor(
	  	public _categoriasSerService:CategoriasServService,
	    public dialogRef: MatDialogRef<DialogCategoriaSer>,
	    @Inject(MAT_DIALOG_DATA) public categoriaSer: DialogData) {}


	  onNoClick(): void {
	    this.dialogRef.close();
	  }

	  onEditClick() {
  	swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
			console.log(this.categoriaSer);
		  	this._categoriasSerService.actualizaCategoriaSer(this.categoriaSer).subscribe(
					response=>{
						if (!response) {
							swal("Error", "Hubo un problema.", "error")
						}else{

							swal(
							  'Modificado',
							  'Se ha modificado el curso.',
							  'success'
							);
							this.dialogRef.close();
						}
					},error=>{
					var errorMensaje = <any>error;
					if (errorMensaje != null) {
						var body = JSON.parse(error._body);
						console.log(error)
						}
					}
				);
		  }
	  });

	}

	  

}

@Component({
	  selector: 'app-registrocategoriasserv-dialog',
	  templateUrl: './popupregcategoriasser.html',
	  styleUrls: ['./categorias.component.css'],
	  providers: [CategoriasServService]
	})
	export class DialogregistroCategoriaSer {

	  constructor(
	  	public _categoriasSerService:CategoriasServService,
	    public dialogRef: MatDialogRef<DialogregistroCategoriaSer>,
	    @Inject(MAT_DIALOG_DATA) public categoriaSer: DialogData) {}


	  onNoClick(): void {
	    this.dialogRef.close();
	  }

	  onRegisterClick(){
		this._categoriasSerService.añadirCategoriaSer(this.categoriaSer).subscribe(
			response=>{
				let categoriaSer = response.idCategoriaservicio_Servicio;
				this.categoriaSer = categoriaSer;
				console.log(this.categoriaSer);
				if (!categoriaSer) {
					// this.alertRegister = 'Error al registrarse';
				}else{
					swal("Exito", response.message, "success");
					this.dialogRef.close();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					// this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
	}
}
