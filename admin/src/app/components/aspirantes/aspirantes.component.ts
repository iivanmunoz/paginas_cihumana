import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { AspiranteService } from '../../services/aspirantes.service';
import { AdministradorService } from '../../services/administrador.service';
import { AspirantesModel } from '../../models/aspirantes.model';
import { GLOBAL } from '../../services/global';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

export interface DialogData {
  idAspirante: string;
  nombreAspirante: string;
  apellidopaternoAspirante:string,
  apellidomaternoAspirante:string,
  correoAspirante:string,
  telefonoAspirante:string,
  cvAspirante:string,
  ocupacionAspirante:string,
  statusAspirante:string,
  fechaAspirante:string,
}


@Component({
  selector: 'app-aspirantes',
  templateUrl: './aspirantes.component.html',
  styleUrls: ['./aspirantes.component.css'],
  providers:[AdministradorService, AspiranteService]
})



export class AspirantesComponent implements OnInit {
	public titulo:string;
	public aspirante:AspirantesModel[];
	public url:string;
	public identity;
	public nombreAspirante;
  public idAspirante;
  public apellidopaternoAspirante;
  public apellidomaternoAspirante;
  public correoAspirante;
  public telefonoAspirante;
  public cvAspirante;
  public ocupacionAspirante;
  public statusAspirante;
  public fechaAspirante;


  constructor(
  	private _route: ActivatedRoute,
  	private _router: Router,
	private _administradorService:AdministradorService,
	private _aspiranteService:AspiranteService,
	public dialog: MatDialog

		) {
		
		this.titulo = 'Aspirantes';
		// this.aspirante = new AspirantesModel('','','','','','','','','','');
		this.url = GLOBAL.url;
		this.identity = _administradorService.getIdentity();
		
	}

	public openAspirante(idAspirante,nombreAspirante,apellidopaternoAspirante,apellidomaternoAspirante,correoAspirante,telefonoAspirante,cvAspirante,ocupacionAspirante,statusAspirante,fechaAspirante):void{
    this.idAspirante = idAspirante;
    this.nombreAspirante = nombreAspirante;
    this.apellidopaternoAspirante = apellidopaternoAspirante;
    this.apellidomaternoAspirante = apellidomaternoAspirante;
    this.correoAspirante = correoAspirante;
    this.telefonoAspirante = telefonoAspirante;
    this.cvAspirante = cvAspirante;
    this.ocupacionAspirante = ocupacionAspirante;
    this.statusAspirante = statusAspirante;
    this.fechaAspirante= fechaAspirante;
    let dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '400px',
      data:{
        idAspirante:this.idAspirante,
        nombreAspirante:this.nombreAspirante,
        apellidopaternoAspirante:this.apellidopaternoAspirante,
        apellidomaternoAspirante:this.apellidomaternoAspirante,
        correoAspirante:this.correoAspirante,
        telefonoAspirante:this.telefonoAspirante,
        cvAspirante:this.cvAspirante,
        ocupacionAspirante:this.ocupacionAspirante,
        statusAspirante:this.statusAspirante,
        fechaAspirante:this.fechaAspirante
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }


  ngOnInit() {
  	this.getAspirantes();
  }

  public eliminarAspirante(idAspirante){
    this._aspiranteService.deleteAspirante(idAspirante).subscribe(response=>{
        if(!response.aspirante){
          this._router.navigate(['/']);
        }
        this.getAspirantes();
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
  }

  public desactivarAspirante(idAspirante){
    this._aspiranteService.deactivateAspirante(idAspirante).subscribe(response=>{
        if(!response.statusAspirante){
          this._router.navigate(['/']);
        }
        this.getAspirantes();
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
  }

  public activarAspirante(idAspirante){
    this._aspiranteService.activateAspirante(idAspirante).subscribe(response=>{
        if(!response.statusAspirante){
          this._router.navigate(['/']);
        }
        this.getAspirantes();
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
  }

  getAspirantes(){
  	this._route.params.forEach((params: Params)=>{
  		this._aspiranteService.getAspirantes().subscribe(response=>{
  			if(!response.aspirantes){
  				this._router.navigate(['/']);
  			}else{
  				this.aspirante = response.aspirantes; 
          console.log(this.aspirante);                  
  			}
  		},
  		error => {
  			var errorMessage = <any>error;
  			if(errorMessage != null){
  				var body = JSON.parse(error._body);
  				console.log(error);
  			}
  		}
  		); 

  	});
  }
}



@Component({
  selector: 'app-aspirantes-dialog',
  templateUrl: './popup.html',
  styleUrls: ['./aspirantes.component.css']
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}