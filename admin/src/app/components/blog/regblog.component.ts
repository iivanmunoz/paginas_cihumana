import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { BlogModel } from '../../models/blog.model';
import { CategoriasBlogService } from '../../services/categoriaBlog.service';
import { CategoriasBlogModel } from '../../models/categoriasBlog.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

@Component({
  selector: 'app-blog',
  templateUrl: './regblog.component.html',
  styleUrls: ['./regblog.component.css'],
  providers:[BlogService, CategoriasBlogService]
})
export class RegblogComponent implements OnInit {

	public blog:BlogModel;
	public alertRegister;
	public categoriaBlog:CategoriasBlogModel;
	public d = new Date();

	constructor(
		private _blogService:BlogService,
		private _route: ActivatedRoute,
		public _categoriasblogService:CategoriasBlogService,
		private _router: Router) {
		this.blog = new BlogModel('','','default.png','','', this.d ,'1');
	}

	ngOnInit() {
		this.getCategoriasBlog();
	}

	getCategoriasBlog() {
	  	this._route.params.forEach((params:Params) =>{
	  		this._categoriasblogService.verCategoriasBlog().subscribe(
	  			response=>{
					if (!response) {
						this.alertRegister = 'Error al cargar';
					}else{
						this.categoriaBlog = response.CatBlog,
						console.log(response.categoriablog);
					}
				},error=>{
					var errorMensaje = <any>error;
					if (errorMensaje != null) {
						var body = JSON.parse(error._body);
						this.alertRegister = body.message;
						console.log(error)
					}
				}
	  		);
	  	});
	  }
	
	onSubmitRegister(){
		this._blogService.crea(this.blog).subscribe(
			response=>{
				let blog = response.idNoticiablog;
				this.blog = blog;
				if (!blog) {
					swal("Error", "Hubo un problema.", "error");
				}else{
					swal("Exito", response.message, "success");
					this._router.navigate(['/blog']);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
	}

}
