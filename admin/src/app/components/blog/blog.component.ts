import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { BlogModel } from '../../models/blog.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers:[BlogService]
})
export class BlogComponent implements OnInit {

  public titulo:string;
	public blog:BlogModel;
	public alertRegister;
	public url:string;
	public next_page;
	public prev_page;

	@Output() aparecer = new EventEmitter;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		public _blogService:BlogService,
		public dialog: MatDialog
		) {
		this.titulo = 'Cursos';
		this.url = GLOBAL.url;
		this.next_page = 1;
		this.prev_page =1;
		
	}

  ngOnInit() {

  	this.getBlog();
  	
  }

  getBlog() {
  	this._route.params.forEach((params:Params) =>{
  		let page = +params['page'];
  		if (!page) {
  			page = 1;
  		}else{
  			this.next_page = page + 1;
  			this.prev_page = page - 1;

  			if (this.prev_page ==0) {
  				this.prev_page = 1;
  			}
  		}
  		this._blogService.getBlog(page).subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.blog = response.Noticias;
					for (var i = 0; i < response.Noticias.length; ++i) {
				  		let fecha = new Date(this.blog[i].fechaNoticiablog);
				           //1 test       
				           let month = new Array();
				              month[0] = "Enero";
				              month[1] = "Febrero";
				              month[2] = "Marzo";
				              month[3] = "Abril";
				              month[4] = "Mayo";
				              month[5] = "Junio";
				              month[6] = "Julio";
				              month[7] = "Agosto";
				              month[8] = "Septiembre";
				              month[9] = "Octubre";
				              month[10] = "Noviembre";
				              month[11] = "Diciembre";
				              let m = month[fecha.getMonth()];
				              let d = fecha.getDate();
				              let y = fecha.getFullYear();
				              this.blog[i].fechaNoticiablog = (d+' de '+m+' del '+y);				              
				  	}
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
  	});
  }

  public confirmado;

   elimConf(idNoticiablog){
  	swal({
  		title: '¿Seguro?',
  		text: "Se dará de baja la noticia.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, desactiva la noticia.'
	}).then((result) => {
		if (result.value) {
		  this._blogService.dabaja(idNoticiablog).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Desactivado',
					  'Se ha desactivado la noticia.',
					  'success'
					);
					this.getBlog();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }


  regreConf(idNoticiablog){
  	swal({
  		title: '¿Seguro?',
  		text: "Se dará de alta la noticia.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, activa la noticia.'
	}).then((result) => {
		if (result.value) {
		  this._blogService.daalta(idNoticiablog).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Activado',
					  'Se ha activado la noticia.',
					  'success'
					);
					this.getBlog();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

}