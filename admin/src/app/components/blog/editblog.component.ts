import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { BlogService } from '../../services/blog.service';
import { BlogModel } from '../../models/blog.model';
import { CategoriasBlogService } from '../../services/categoriaBlog.service';
import { CategoriasBlogModel } from '../../models/categoriasBlog.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

@Component({
  selector: 'app-editblog',
  templateUrl: './editblog.component.html',
  styleUrls: ['./editblog.component.css'],
  providers:[BlogService, CategoriasBlogService]
})
export class EditblogComponent implements OnInit {

  public blog:BlogModel;
  public categoriaBlog:CategoriasBlogModel;
  public alertRegister;
  public url:string;
  public filesToUpload: Array<File>;
  public token;
  public dir:string;

	constructor(
		private _blogService:BlogService,
		private _route: ActivatedRoute,
		public _categoriasblogService:CategoriasBlogService,
		private _router: Router) {
		this.blog = new BlogModel ('','','','','', null ,'');
		this.url = GLOBAL.url;
		this.dir = GLOBAL.url;
	}

  ngOnInit() {
  	this.veBlog();
  	this.getCategoriasBlog();

  	function archivo(evt) {
	      var files = evt.target.files; // FileList object
	        //Obtenemos la imagen del campo "file". 
	        for (var i = 0, f; f = files[i]; i++) {         
	           //Solo admitimos imágenes.
	           if (!f.type.match('image.*')) {
	           	continue;
	           }
	           var reader = new FileReader();	           
	           reader.onload = (function(theFile) {
	           	return function(e) {
	               // Creamos la imagen.
	               document.getElementById("list").innerHTML = ['<img style="width: 50%" class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');

	           };
	       })(f);
	       reader.readAsDataURL(f);
	   }
	}
	document.getElementById('files').addEventListener('change', archivo, false);
  }

  getCategoriasBlog() {
	  	this._route.params.forEach((params:Params) =>{
	  		this._categoriasblogService.verCategoriasBlog().subscribe(
	  			response=>{
					if (!response) {
						this.alertRegister = 'Error al cargar';
					}else{
						this.categoriaBlog = response.CatBlog,
						console.log(response.categoriablog);
					}
				},error=>{
					var errorMensaje = <any>error;
					if (errorMensaje != null) {
						var body = JSON.parse(error._body);
						this.alertRegister = body.message;
						console.log(error)
					}
				}
	  		);
	  	});
	  }

  onSubmitRegister(){
		swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
		  	this._blogService.actualiza(this.blog).subscribe(
		      response=>{
		        if (!response) {
		        	swal("Error", "Hubo un problema.", "error");
		        }else{
		          if (!this.filesToUpload) {
		            if (this.blog.imagenNoticiablog != 'default.png') {
		              console.log(this.blog.imagenNoticiablog);
		              this.blog.imagenNoticiablog = this.blog.imagenNoticiablog;
		            }
		            localStorage.setItem('identity', JSON.stringify(this.blog));
		          }else{
		            this.makeFileRquest(this.url+'subeImagenB/'+ this.blog.idNoticiablog,
		              [], this.filesToUpload).then(
		              (result:any)=>{
		                this.blog.imagenNoticiablog = result.image.imagenNoticiablog
		                console.log(result.image.imagenNoticiablog);
		                localStorage.setItem('identity', JSON.stringify(this.blog));
		                console.log(this.blog);
		              }
		              ).catch(error=>{
		                console.log(`${error}`);
		              })
		            }
		            swal("Exito", response.message, "success");
		          }
		        },error=>{
		          var errorMensaje = <any>error;
		          if (errorMensaje != null) {
		            var body = JSON.parse(error._body);
		            console.log(error)
		          }
		        }
		        )
		  }
	  });

	}

	makeFileRquest(url:string,params:Array<string>, files:Array<File>){
	    return new Promise(function(resolve,reject){
	      var formData:any = new FormData();
	      var xhr = new XMLHttpRequest();
	      for (var i = 0; i < files.length; i++) {
	        formData.append('imagenNoticiablog',files[i],files[i].name)
	      }
	      xhr.onreadystatechange = function(){
	        if (xhr.readyState == 4) {
	          if (xhr.status == 200) {
	            resolve(JSON.parse(xhr.response));  
	          }else{
	            reject(xhr.response);
	          }
	        }
	      }
	      xhr.open('PUT',url,true);
	      xhr.send(formData);
	    })
	  }

	fileChangeEvent(fileInput:any){
		this.filesToUpload = <Array<File>>fileInput.target.files;
	}


  public veBlog() {
    this._route.params.forEach((params: Params)=>{
      let idNoticiablog=params["idNoticiablog"];
      this._blogService.verBlog(idNoticiablog).subscribe(response=>{
        if(!response){
          this._router.navigate(['/']);
        }else{
          this.blog = response.Noticias[0];
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
    });
  }
}