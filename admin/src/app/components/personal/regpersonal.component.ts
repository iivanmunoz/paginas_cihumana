import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { PersonalService } from '../../services/personal.service';
import { PersonalModel } from '../../models/personal.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from'sweetalert2';

@Component({
  selector: 'app-regpersonal',
  templateUrl: './regpersonal.component.html',
  styleUrls: ['./regpersonal.component.css'],
  providers:[PersonalService]
})
export class RegpersonalComponent implements OnInit {

  	public personal:PersonalModel;
	public alertRegister;

	@Output() aparecer = new EventEmitter;

	constructor(
		private _personalService:PersonalService,
		private _route: ActivatedRoute,
		private _router: Router) {
		this.personal = new PersonalModel('','','','','','','','','');
	}

	ngOnInit() {

	}
	
	onSubmitRegister(){
		this._personalService.registerPersonal(this.personal).subscribe(
			response=>{
				let personal = response.idPersonal;
				this.personal = personal;
				if (!personal) {
					this.alertRegister = 'Error al registrarse';
				}else{
					swal("Exito", response.message, "success");
					this._router.navigate(['/personal']);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
	}

}
