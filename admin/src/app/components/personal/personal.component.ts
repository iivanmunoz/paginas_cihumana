import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { PersonalService } from '../../services/personal.service';
import { PersonalModel } from '../../models/personal.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css'],
  providers:[PersonalService]
})
export class PersonalComponent implements OnInit {

	public titulo:string;
	public personal:PersonalModel;
	public alertRegister;
	public url:string;


  constructor(
  	private _route: ActivatedRoute,
		private _router: Router,
		public _personalService:PersonalService,
		public dialog: MatDialog
  	) {
  	this.titulo = 'Personal';
	this.url = GLOBAL.url;
  }

  ngOnInit() {

  	this.getPersonal();
  }

  getPersonal(){
    this._route.params.forEach((params: Params)=>{
      this._personalService.getPersonal().subscribe(response=>{
        if(!response.personal){
          this._router.navigate(['/']);
        }else{
          this.personal = response.personal;
          console.log(this.personal)
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 

    });
  }

  elimConf(idPersonal){
    swal({
      title: '¿Seguro?',
      text: "Se dará de baja el personal.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, desactiva el personal.'
  }).then((result) => {
    if (result.value) {
      this._personalService.dabaja(idPersonal).subscribe(
        response=>{
        if (!response) {
          this.alertRegister = 'Error al encontrar';
        }else{
          swal(
            'Desactivado',
            'Se ha desactivado el personal.',
            'success'
          );
          this.getPersonal();
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          this.alertRegister = body.message;
          console.log(error)
        }
      }
      );
    }
  });
  }


  regreConf(idPersonal){
    swal({
      title: '¿Seguro?',
      text: "Se dará de alta el personal.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, activa el personal.'
  }).then((result) => {
    if (result.value) {
      this._personalService.daalta(idPersonal).subscribe(
        response=>{
        if (!response) {
          this.alertRegister = 'Error al encontrar';
        }else{
          swal(
            'Activado',
            'Se ha activado el personal.',
            'success'
          );
          this.getPersonal();
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          this.alertRegister = body.message;
          console.log(error)
        }
      }
      );
    }
  });
  }

}