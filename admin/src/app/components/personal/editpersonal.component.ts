import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { PersonalService } from '../../services/personal.service';
import { PersonalModel } from '../../models/personal.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from'sweetalert2';

@Component({
  selector: 'app-editpersonal',
  templateUrl: './editpersonal.component.html',
  styleUrls: ['./editpersonal.component.css'],
  providers:[PersonalService]
})
export class EditpersonalComponent implements OnInit {

  public personal:PersonalModel;
  public alertRegister;
  public url:string;
  public filesToUpload: Array<File>;
  public token;
  public dir:string;

	constructor(
		private _personalService:PersonalService,
		private _route: ActivatedRoute,
		private _router: Router) {
		this.personal = new PersonalModel ('','','','','','','','','');
		this.url = GLOBAL.url;
		this.dir = GLOBAL.url;
	}

  ngOnInit() {
  	this.vePersonal();

  	function archivo(evt) {
	      var files = evt.target.files; // FileList object
	        //Obtenemos la imagen del campo "file". 
	        for (var i = 0, f; f = files[i]; i++) {         
	           //Solo admitimos imágenes.
	           if (!f.type.match('image.*')) {
	           	continue;
	           }
	           var reader = new FileReader();	           
	           reader.onload = (function(theFile) {
	           	return function(e) {
	               // Creamos la imagen.
	               document.getElementById("list").innerHTML = ['<img style="width: 50%" class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');

	           };
	       })(f);
	       reader.readAsDataURL(f);
	   }
	}
	document.getElementById('files').addEventListener('change', archivo, false);
  }

  onSubmitRegister(){
		swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
		  	this._personalService.updatePersonal(this.personal).subscribe(
		      response=>{
		        if (!response.personal) {
		        	swal("Error", "Hubo un problema.", "error");
		        }else{
		          if (!this.filesToUpload) {
		            if (this.personal.fotoPersonal != 'default.png') {
		              console.log(this.personal.fotoPersonal);
		              this.personal.fotoPersonal = this.personal.fotoPersonal;
		            }
		            localStorage.setItem('identity', JSON.stringify(this.personal));
		          }else{
		            this.makeFileRquest(this.url+'subeImagenPersonal/'+ this.personal.idPersonal,
		              [], this.filesToUpload).then(
		              (result:any)=>{
		                this.personal.fotoPersonal = result.image.fotoPersonal
		                console.log(result.image.fotoPersonal);
		                localStorage.setItem('identity', JSON.stringify(this.personal));
		                console.log(this.personal);
		              }
		              ).catch(error=>{
		                console.log(`${error}`);
		              })
		            }
		            swal("Exito", response.message, "success");
		          }
		        },error=>{
		          var errorMensaje = <any>error;
		          if (errorMensaje != null) {
		            var body = JSON.parse(error._body);
		            console.log(error)
		          }
		        }
		        )
		  }
	  });

	}

	makeFileRquest(url:string,params:Array<string>, files:Array<File>){
	    return new Promise(function(resolve,reject){
	      var formData:any = new FormData();
	      var xhr = new XMLHttpRequest();
	      for (var i = 0; i < files.length; i++) {
	        formData.append('fotoPersonal',files[i],files[i].name)
	      }
	      xhr.onreadystatechange = function(){
	        if (xhr.readyState == 4) {
	          if (xhr.status == 200) {
	            resolve(JSON.parse(xhr.response));  
	          }else{
	            reject(xhr.response);
	          }
	        }
	      }
	      xhr.open('PUT',url,true);
	      xhr.send(formData);
	    })
	  }

	fileChangeEvent(fileInput:any){
		this.filesToUpload = <Array<File>>fileInput.target.files;
	}


  public vePersonal() {
    this._route.params.forEach((params: Params)=>{
      let idPersonal=params["idPersonal"];
      this._personalService.vePersonal(idPersonal).subscribe(response=>{
        if(!response){
          this._router.navigate(['/']);
        }else{
          this.personal = response.personal[0],
          console.log(this.personal)
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
    });
  }

}
