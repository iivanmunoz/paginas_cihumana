import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ClientesService } from '../../services/clientes.service';
import { ClienteModel } from '../../models/clientes.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from'sweetalert2';

@Component({
  selector: 'app-editcliente',
  templateUrl: './editcliente.component.html',
  styleUrls: ['./editcliente.component.css'],
  providers:[ClientesService]
})
export class EditClienteComponent implements OnInit {

  public client:ClienteModel;
  public alertRegister;
  public url:string;
  public filesToUpload: Array<File>;
  public token;

	constructor(
		private _clienteService:ClientesService,
		private _route: ActivatedRoute,
		private _router: Router) {
		this.url = GLOBAL.url;
		this.client = new ClienteModel ('','','','');
	}

  ngOnInit() {
  	this.getCliente();
  	function archivo(evt) {
	      var files = evt.target.files; // FileList object
	        //Obtenemos la imagen del campo "file". 
	        for (var i = 0, f; f = files[i]; i++) {         
	           //Solo admitimos imágenes.
	           if (!f.type.match('image.*')) {
	           	continue;
	           }
	           var reader = new FileReader();	           
	           reader.onload = (function(theFile) {
	           	return function(e) {
	               // Creamos la imagen.
	               document.getElementById("list").innerHTML = ['<img style="width: 50%" class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');

	           };
	       })(f);
	       reader.readAsDataURL(f);
	   }
	}
	document.getElementById('files').addEventListener('change', archivo, false);
  }

  onSubmitRegister(){
		this._route.params.forEach((params: Params) =>{
  		let idCliente = params['idCliente'];
		swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
		  	this._clienteService.actualiza(this.client).subscribe(
					response=>{						
						if (!response) {
							swal("Error", "Hubo un problema.", "error")
						}else{
							if (!this.filesToUpload) {
						if (this.client.fotoCliente != 'default.png') {
							this.client.fotoCliente = this.client.fotoCliente;
						}
						localStorage.setItem('identity', JSON.stringify(this.client));						
					}else{
						this.makeFileRquest(this.url+'subeImagenCliente/'+ idCliente,
							[], this.filesToUpload).then(
							(result:any)=>{
								this.client.fotoCliente = result.image.fotoCliente;								
								localStorage.setItem('identity', JSON.stringify(this.client));
								let urlImagen = this.url+'getImageFileCliente/'+this.client.fotoCliente;
								console.log(this.client);								
							}
							).catch(error=>{
								console.log(`${error}`);
							})
						}

						// this.alertUpdate = 'El usuario se ha actualizado';
						swal(
							  'Modificado',
							  'Se ha modificado el cliente.',
							  'success'
							);
						this._router.navigate(['/clientes']);
					}
					},error=>{
						var errorMensaje = <any>error;
						if (errorMensaje != null) {
							var body = JSON.parse(error._body);
							console.log(error)
						}
					}
				);
		  }
	  });
	});

	}


  public getCliente() {
  	this._route.params.forEach((params: Params) =>{
  		let idCliente = params['idCliente'];
  		this._clienteService.verCliente(idCliente).subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.client = response.client[0],
					console.log(this.client);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
  	});
  }

		fileChangeEvent(fileInput:any){
				this.filesToUpload = <Array<File>>fileInput.target.files;
				$('#info').text(this.filesToUpload[0].name);
			}

			makeFileRquest(url:string,params:Array<string>, files:Array<File>){
				let token = this.token;
				return new Promise(function(resolve,reject){
					var formData:any = new FormData();
					var xhr = new XMLHttpRequest();
					for (var i = 0; i < files.length; i++) {
						formData.append('fotoCliente',files[i],files[i].name)
					}
					xhr.onreadystatechange = function(){
						if (xhr.readyState == 4) {
							if (xhr.status == 200) {
								resolve(JSON.parse(xhr.response));	
							}else{
								reject(xhr.response);
							}
						}
					}
					xhr.open('PUT',url,true);
					xhr.setRequestHeader('Authorization',token);

					    xhr.upload.onprogress = function(e) {
					      if (e.lengthComputable) {
					        var percentage = (e.loaded / e.total) * 100;
					        console.log(percentage + "%");
					        $('.progress-bar').css("width",percentage + "%");
					      }
					    };

					    xhr.onerror = function(e) {
					      console.log('Error');
					      console.log(e);
					    };
					    xhr.onload = function() {
					      console.log(this.statusText);
					    };
					xhr.send(formData);
				})
			}

		}

