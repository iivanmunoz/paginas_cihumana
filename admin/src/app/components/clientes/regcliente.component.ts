import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ClientesService } from '../../services/clientes.service';
import { ClienteModel } from '../../models/clientes.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from'sweetalert2';

@Component({
  selector: 'app-regcliente',
  templateUrl: './regcliente.component.html',
  styleUrls: ['./regcliente.component.css'],
  providers:[ClientesService]
})
export class RegclienteComponent implements OnInit {

  	public client:ClienteModel;
	public alertRegister;
	public filesToUpload: Array<File>;
	public token;
	public url:string;

	constructor(
		private _clienteService:ClientesService,
		private _route: ActivatedRoute,
		private _router: Router) {
		this.client = new ClienteModel('','','','');
		this.url = GLOBAL.url;
	}

	ngOnInit() {
		function archivo(evt) {
	      var files = evt.target.files; // FileList object
	        //Obtenemos la imagen del campo "file". 
	        for (var i = 0, f; f = files[i]; i++) {         
	           //Solo admitimos imágenes.
	           if (!f.type.match('image.*')) {
	           	continue;
	           }
	           var reader = new FileReader();	           
	           reader.onload = (function(theFile) {
	           	return function(e) {
	               // Creamos la imagen.
	               document.getElementById("list").innerHTML = ['<img style="width: 50%" class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');

	           };
	       })(f);
	       reader.readAsDataURL(f);
	   }
	}
	document.getElementById('files').addEventListener('change', archivo, false);

	}


	public getCliente() {
  	this._route.params.forEach((params: Params) =>{
  		let idCliente = params['idCliente'];
  		this._clienteService.verCliente(idCliente).subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.client = response.client[0],
					console.log(this.client);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
  	});
  }
	
	onSubmitRegister(){
		this._clienteService.añadirCliente(this.client).subscribe(
			response=>{
				let client = response.idCliente;
				this.client = client;
				if (!client) {
					this.alertRegister = 'Error al registrarse';
					console.log('lel')
				}else{
					if (!this.filesToUpload) {
						console.log('aoida');
						if (this.client.fotoCliente != 'default.png') {
							this.client.fotoCliente = this.client.fotoCliente;
							console.log(this.client.fotoCliente);
						}
						localStorage.setItem('identity', JSON.stringify(this.client));
					}else{
						console.log('aoida3');
						this.makeFileRquest(this.url+'subeImagenCliente/'+this.client,
							[], this.filesToUpload).then(
							(result:any)=>{
								this.client.fotoCliente = result.image.fotoCliente;								
								localStorage.setItem('identity', JSON.stringify(this.client));
								let urlImagen = this.url+'getImageFileCliente/'+this.client.fotoCliente;
								console.log(this.client);
							}
							).catch(error=>{
								console.log(`${error}`);
							})
						}

						 this.alertRegister = 'Se ha enviado correctamente';
						this._router.navigate(['/clientes']);
					}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
	}


	fileChangeEvent(fileInput:any){
		this.filesToUpload = <Array<File>>fileInput.target.files;
		$('#info').text(this.filesToUpload[0].name);
	}

	makeFileRquest(url:string,params:Array<string>, files:Array<File>){
		let token = this.token;
		return new Promise(function(resolve,reject){
			var formData:any = new FormData();
			var xhr = new XMLHttpRequest();
			for (var i = 0; i < files.length; i++) {
				formData.append('fotoCliente',files[i],files[i].name)
			}
			xhr.onreadystatechange = function(){
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
						resolve(JSON.parse(xhr.response));	
					}else{
						reject(xhr.response);
					}
				}
			}
			xhr.open('PUT',url,true);
			xhr.setRequestHeader('Authorization',token);
			xhr.send(formData);
		})
	}

}
