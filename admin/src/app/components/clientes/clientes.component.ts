import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { ClientesService } from '../../services/clientes.service';
import { ClienteModel } from '../../models/clientes.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';

export interface DialogData {
  idCliente: string;
  empresaCliente:string,
  telefonoCliente:string,
  statusCliente:string,
  fotoCliente:string
}

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css'],
  providers: [ClientesService]
})
export class ClientesComponent implements OnInit {

	public titulo:string;
	public client:ClienteModel;
	public identity;
	public token;
	public alertRegister;
	public url:string;
	public idCliente;
	public empresaCliente;
 	  public telefonoCliente;
	  public statusCliente;
	  public fotoCliente;

 constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		public _clienteService:ClientesService,
		public dialog: MatDialog
		) {
		this.titulo = 'Cursos';
		this.url = GLOBAL.url;
		
	}
  ngOnInit() {

  	this.getClientes();
  	
  }

  getCliente() {
  	this._route.params.forEach((params: Params) =>{
  		let idCliente = params['idCliente'];
  		this._clienteService.verCliente(idCliente).subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.client = response.client,
					console.log(response.client);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
  	});
  }

  getClientes() {
  	this._route.params.forEach((params:Params) =>{
  		this._clienteService.verClientes().subscribe(
  			response=>{
				if (!response) {
					this.alertRegister = 'Error al cargar';
				}else{
					this.client = response.client,
					console.log(response.client);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
  	});
  }

  public confirmado;

  elimCliente(idCliente){
  	swal({
  		title: '¿Seguro?',
  		text: "Se desactivará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, desactiva al cliente.'
	}).then((result) => {
		if (result.value) {
		  this._clienteService.dabaja(idCliente).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Desactivado',
					  'Se ha desactivado el cliente.',
					  'success'
					);
					this.getClientes();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }

  regreCliente(idCliente){
  	swal({
  		title: '¿Seguro?',
  		text: "Se activará.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, activa al cliente.'
	}).then((result) => {
		if (result.value) {
		  this._clienteService.daalta(idCliente).subscribe(
		  	response=>{
				if (!response) {
					this.alertRegister = 'Error al encontrar';
				}else{
					swal(
					  'Activado',
					  'Se ha activado el cliente.',
					  'success'
					);
					this.getClientes();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
  		);
		}
	});
  }


  // openDialog(idCliente, empresaCliente, telefonoCliente, statusCliente, fotoCliente): void {
  //   this.idCliente = idCliente;
  //   this.empresaCliente = empresaCliente;
  //   this.telefonoCliente = telefonoCliente;
  //   this.statusCliente = statusCliente;
  //   this.fotoCliente = fotoCliente;
  //   const dialogRef = this.dialog.open(DialogCliente, {
  //     width: '250px',
  //     data: {idCliente: this.idCliente,
  //     	empresaCliente: this.empresaCliente,
  //     	telefonoCliente: this.telefonoCliente,
  //     	statusCliente: this.statusCliente,
  //     	fotoCliente: this.fotoCliente
  //     }
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     this.ngOnInit();
  //   });
  // }

  // irEdi(idCurso, nombreCurso, descripcionCurso, aprendizajeCurso, objetivoCurso, duracionCurso, urlCurso): void {
  // 	this.idCurso = idCurso;
  //   this.nombreCurso = nombreCurso;
  //   this.descripcionCurso = descripcionCurso;
  //   this.aprendizajeCurso = aprendizajeCurso;
  //   this.objetivoCurso = objetivoCurso;
  //   this.duracionCurso = duracionCurso;
  //   this.urlCurso = urlCurso;
  // }

}


@Component({
  selector: 'cliente-dialog',
  templateUrl: 'cliente-dialog.html',
  styleUrls: ['./clientes.component.css'],
  providers:[ClientesService]
})

export class DialogCliente {

  constructor(
  	public _clienteService:ClientesService,
    public dialogRef: MatDialogRef<DialogCliente>,
    @Inject(MAT_DIALOG_DATA) public cliente: DialogData) {}


  onNoClick(): void {
    this.dialogRef.close();
  }

  onEditClick() {
  	swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
		  	this._clienteService.actualiza(this.cliente).subscribe(
					response=>{
						if (!response) {
							swal("Error", "Hubo un problema.", "error")
						}else{
							swal(
							  'Modificado',
							  'Se ha modificado el cliente.',
							  'success'
							);
							this.dialogRef.close();
						}
					},error=>{
						var errorMensaje = <any>error;
						if (errorMensaje != null) {
							var body = JSON.parse(error._body);
							console.log(error)
						}
					}
				);
		  }
	  });

	}


}
