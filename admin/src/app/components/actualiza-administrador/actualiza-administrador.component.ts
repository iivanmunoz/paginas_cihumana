import { Component, OnInit } from '@angular/core';
import { AdministradorService } from '../../services/administrador.service';
import { AdministradorModel } from '../../models/administrador.model';
import { GLOBAL } from '../../services/global';
import {Router, ActivatedRoute, Params} from '@angular/router';
import swal from'sweetalert2';
import * as $ from 'jquery';


@Component({
	selector: 'app-actualiza-administrador',
	templateUrl: './actualiza-administrador.component.html',
	styleUrls: ['./actualiza-administrador.component.css'],
	providers:[AdministradorService]
})
export class ActualizaAdministradorComponent implements OnInit {
	public titulo:string;
	public identity;
	public token;
	public administrador:AdministradorModel;
	public alertUpdate;
	public url:string;
	public filesToUpload: Array<File>;


	constructor(
		private _administradorService:AdministradorService,
		private _route: ActivatedRoute,
  	private _router: Router
		) {
		
		this.titulo = 'Actualizar Mis Datos';

		// LocalStorage
		this.identity = this._administradorService.getIdentity();
		this.token = this._administradorService.getToken();

		this.administrador = this.identity;
		this.url = GLOBAL.url;
		
	}

	ngOnInit() {
		function archivo(evt) {
	      var files = evt.target.files; // FileList object
	        //Obtenemos la imagen del campo "file". 
	        for (var i = 0, f; f = files[i]; i++) {         
	           //Solo admitimos imágenes.
	           if (!f.type.match('image.*')) {
	           	continue;
	           }
	           var reader = new FileReader();	           
	           reader.onload = (function(theFile) {
	           	return function(e) {
	               // Creamos la imagen.
	               document.getElementById("list").innerHTML = ['<img style="width: 50%" class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');

	           };
	       })(f);
	       reader.readAsDataURL(f);
	   }
	}
	document.getElementById('files').addEventListener('change', archivo, false);


}
onSubmit(){
		// console.log(this.administrador);
		this._administradorService.updateAdmin(this.administrador).subscribe(

			response=>{
				if (!response.administrador) {
					this.alertUpdate = 'El usuario no se ha actualizado';
				}else{
					// this.administrador = response.administrador[0];
					if (!this.filesToUpload) {
						if (this.administrador.fotoAdministrador != 'default.png') {
							this.administrador.fotoAdministrador = this.administrador.fotoAdministrador;

						}
						localStorage.setItem('identity', JSON.stringify(this.administrador));
						document.getElementById('nombreAdministrador').innerHTML = this.administrador.nombreAdministrador+" "+this.administrador.apellidopaternoAdministrador + " " + this.administrador.apellidomaternoAdministrador;
					}else{
						this.makeFileRquest(this.url+'subeImagen/'+ this.administrador.idAdministrador,
							[], this.filesToUpload).then(
							(result:any)=>{
								this.administrador.fotoAdministrador = result.image.fotoAdministrador;								
								localStorage.setItem('identity', JSON.stringify(this.administrador));
								let urlImagen = this.url+'getImagen/'+this.administrador.fotoAdministrador;
								document.getElementById('fotoAdministrador').setAttribute('src',urlImagen);
								document.getElementById('nombreAdministrador').innerHTML = this.administrador.nombreAdministrador+" "+this.administrador.apellidopaternoAdministrador + " " + this.administrador.apellidomaternoAdministrador;
								console.log(this.administrador);								
							}
							).catch(error=>{
								console.log(`${error}`);
							})
						}

						// this.alertUpdate = 'El usuario se ha actualizado';
						swal("Exito", response.message, "success");
						this._router.navigate(['/']);
					}
				},error=>{
					var errorMensaje = <any>error;
					if (errorMensaje != null) {
						var body = JSON.parse(error._body);
						this.alertUpdate = body.message;
						console.log(error)
					}
				}
				)
	}

	fileChangeEvent(fileInput:any){
		this.filesToUpload = <Array<File>>fileInput.target.files;
		$('#info').text(this.filesToUpload[0].name);
	}

	makeFileRquest(url:string,params:Array<string>, files:Array<File>){
		let token = this.token;
		return new Promise(function(resolve,reject){
			var formData:any = new FormData();
			var xhr = new XMLHttpRequest();
			for (var i = 0; i < files.length; i++) {
				formData.append('fotoAdministrador',files[i],files[i].name)
				console.log(files);
			}
			xhr.onreadystatechange = function(){
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
						resolve(JSON.parse(xhr.response));	
					}else{
						reject(xhr.response);
					}
				}
			}
			xhr.open('PUT',url,true);
			xhr.setRequestHeader('Authorization',token);

			    xhr.upload.onprogress = function(e) {
			      if (e.lengthComputable) {
			        var percentage = (e.loaded / e.total) * 100;
			        console.log(percentage + "%");
			        $('.progress-bar').css("width",percentage + "%");
			      }
			    };

			    xhr.onerror = function(e) {
			      console.log('Error');
			      console.log(e);
			    };
			    xhr.onload = function() {
			      console.log(this.statusText);
			    };
			xhr.send(formData);
		})
	}

}
