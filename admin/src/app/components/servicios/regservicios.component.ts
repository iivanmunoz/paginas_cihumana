import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { ServicioService } from '../../services/servicio.service';
import { ServicioModel } from '../../models/servicio.model';
import { CategoriasServService } from '../../services/categoriaSer.service';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

@Component({
  selector: 'app-regservicios',
  templateUrl: './regservicios.component.html',
  styleUrls: ['./regservicios.component.css'],
  providers:[ServicioService, CategoriasServService]
})
export class RegserviciosComponent implements OnInit {

  	public servicio:ServicioModel;
	public alertRegister;
	public categoriaSer;

	@Output() aparecer = new EventEmitter;

	constructor(
		private _servicioService:ServicioService,
		private _categoriasSerService:CategoriasServService,
		private _route: ActivatedRoute,
		private _router: Router) {
		this.servicio = new ServicioModel('','','','','','');
	}

	ngOnInit() {
		this.getCategoriasSer();
	}

	getCategoriasSer() {
	  	this._route.params.forEach((params:Params) =>{
	  		this._categoriasSerService.verCategoriasSer().subscribe(
	  			response=>{
					if (!response) {
						this.alertRegister = 'Error al cargar';
					}else{
						this.categoriaSer = response.CatServ;
					}
				},error=>{
					var errorMensaje = <any>error;
					if (errorMensaje != null) {
						var body = JSON.parse(error._body);
						this.alertRegister = body.message;
						console.log(error)
					}
				}
	  		);
	  	});
  	}
	
	onSubmitRegister(){
		this._servicioService.creaServicio(this.servicio).subscribe(
			response=>{
				let servicio = response.idServicio;
				this.servicio = servicio;
				if (!servicio) {
					this.alertRegister = 'Error al registrarse';
				}else{
					swal("Exito", response.message, "success");
					this._router.navigate(['/serv']);
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
	}

}
