import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { ServicioService } from '../../services/servicio.service';
import { ServicioModel } from '../../models/servicio.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css'],
  providers:[ServicioService]
})
export class ServiciosComponent implements OnInit {

  public titulo:string;
	public servicio:ServicioModel;
	public alertRegister;
	public url:string;


  constructor(
  	private _route: ActivatedRoute,
		private _router: Router,
		public _servicioService:ServicioService,
		public dialog: MatDialog
  	) {
  	this.titulo = 'Servicio';
	this.url = GLOBAL.url;
  }

  ngOnInit() {

  	this.getPersonal();
  }

  getPersonal(){
    this._route.params.forEach((params: Params)=>{
      this._servicioService.getServicios().subscribe(response=>{
        if(!response.Servicio){
          this._router.navigate(['/']);
        }else{
          this.servicio = response.Servicio;
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 

    });
  }

  elimConf(idServicio){
    swal({
      title: '¿Seguro?',
      text: "Se dará de baja el servicio.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, desactiva el servicio.'
  }).then((result) => {
    if (result.value) {
      this._servicioService.dabaja(idServicio).subscribe(
        response=>{
        if (!response) {
          this.alertRegister = 'Error al encontrar';
        }else{
          swal(
            'Desactivado',
            'Se ha desactivado el servicio.',
            'success'
          );
          this.getPersonal();
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          this.alertRegister = body.message;
          console.log(error)
        }
      }
      );
    }
  });
  }


  regreConf(idServicio){
    swal({
      title: '¿Seguro?',
      text: "Se dará de alta el servicio.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, activa el servicio.'
  }).then((result) => {
    if (result.value) {
      this._servicioService.daalta(idServicio).subscribe(
        response=>{
        if (!response) {
          this.alertRegister = 'Error al encontrar';
        }else{
          swal(
            'Activado',
            'Se ha activado el servicio.',
            'success'
          );
          this.getPersonal();
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          this.alertRegister = body.message;
          console.log(error)
        }
      }
      );
    }
  });
  }

}
