import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { ServicioService } from '../../services/servicio.service';
import { ServicioModel } from '../../models/servicio.model';
import { CategoriasServService } from '../../services/categoriaSer.service';
import { puntoServicioService } from '../../services/puntoservicio.service';
import { puntoServicioModel } from '../../models/puntoservicio.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

export interface DialogData {
  descripcionPuntoservicio: string;
  idServicio_Puntoservicio: string;
}

@Component({
  selector: 'app-editservicios',
  templateUrl: './editservicios.component.html',
  styleUrls: ['./editservicios.component.css'],
  providers:[ServicioService, puntoServicioService, CategoriasServService]
})
export class EditserviciosComponent implements OnInit {

  public servicio:ServicioModel;
  public puntoservicio:puntoServicioModel;
  public puntoservicio2:puntoServicioModel[];
  public alertRegister;
  public url:string;
  public filesToUpload: Array<File>;
  public token;
  public idServicio_Puntoservicio;
  public descripcionPuntoservicio;
  public categoriaSer;

	constructor(
		private _servicioService:ServicioService,
		private _puntoservicioService:puntoServicioService,
    private _categoriasSerService:CategoriasServService,
		private _route: ActivatedRoute,
		private _router: Router,
		public dialog: MatDialog) {
		this.servicio = new ServicioModel ('','','','','','');
		this.puntoservicio = new puntoServicioModel ('','','','');
		this.url = GLOBAL.url;
	}

  ngOnInit() {
  	this.veServicio();
  	this.buscapuntoServicios();
  	this.getCategoriasSer();
  }

  getCategoriasSer() {
      this._route.params.forEach((params:Params) =>{
        this._categoriasSerService.verCategoriasSer().subscribe(
          response=>{
          if (!response) {
            this.alertRegister = 'Error al cargar';
          }else{
            this.categoriaSer = response.CatServ;
          }
        },error=>{
          var errorMensaje = <any>error;
          if (errorMensaje != null) {
            var body = JSON.parse(error._body);
            this.alertRegister = body.message;
            console.log(error)
          }
        }
        );
      });
    }

  onSubmitRegister(){
		swal({
  		title: '¿Seguro?',
  		text: "Se modificará permanentemente.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, estoy seguro'
	}).then((result) => {
		if (result.value) {
			this.ActualizaPServ();
		  	this.ActualizaServ();
		  }
	  });
	}
	
	public ActualizaServ() {
		this._servicioService.updateServicio(this.servicio).subscribe(
					response=>{
						if (!response) {
							swal("Error", "Hubo un problema.", "error")
						}else{
							swal(
							  'Modificado',
							  'Se ha modificado el servicio.',
							  'success'
							);
							this._router.navigate(['/serv']);
						}
					},error=>{
						var errorMensaje = <any>error;
						if (errorMensaje != null) {
							var body = JSON.parse(error._body);
							console.log(error)
						}
					}
				);
	}

	public ActualizaPServ() {
		for (var i = 0; i <= this.puntoservicio2.length - 1; i++) {
			this._puntoservicioService.actualizapuntoServicio(this.puntoservicio2[i]).subscribe(
					response=>{
						if (!response) {
							console.log("Hubo un problema.");
						}else{
							console.log("Exito");
						}
					},error=>{
						var errorMensaje = <any>error;
						if (errorMensaje != null) {
							var body = JSON.parse(error._body);
							console.log(error)
						}
					}
				);
		}
	}

	

  public veServicio() {
    this._route.params.forEach((params: Params)=>{
      let idServicio=params["idServicio"];
      this._servicioService.veServicio(idServicio).subscribe(response=>{
        if(!response){
          this._router.navigate(['/']);
        }else{
          this.servicio = response.Servicio[0];
          console.log(this.servicio);
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
    });
  }

  public buscapuntoServicios() {
    this._route.params.forEach((params: Params)=>{
      let idServicio=params["idServicio"];
      this._puntoservicioService.buscapuntoServicios(idServicio).subscribe(response=>{
        if(!response){
          console.log('No hay respuesta');
        }else{
          this.puntoservicio = response;
          this.puntoservicio2 = response.PuntosdeServicio;
          console.log(this.puntoservicio2);
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
    });
  }

  elimConf(idPServicio){
    swal({
      title: '¿Seguro?',
      text: "Se dará de baja el servicio.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, desactiva el servicio.'
  }).then((result) => {
    if (result.value) {
      this._puntoservicioService.dabaja(idPServicio).subscribe(
        response=>{
        if (!response) {
          this.alertRegister = 'Error al encontrar';
        }else{
          swal(
            'Desactivado',
            'Se ha desactivado el servicio.',
            'success'
          );
          this.ngOnInit();
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          this.alertRegister = body.message;
          console.log(error)
        }
      }
      );
    }
  });
  }


  regreConf(idPServicio){
    swal({
      title: '¿Seguro?',
      text: "Se dará de alta el servicio.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, activa el servicio.'
  }).then((result) => {
    if (result.value) {
      this._puntoservicioService.daalta(idPServicio).subscribe(
        response=>{
        if (!response) {
          this.alertRegister = 'Error al encontrar';
        }else{
          swal(
            'Activado',
            'Se ha activado el servicio.',
            'success'
          );
          this.ngOnInit();
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          this.alertRegister = body.message;
          console.log(error)
        }
      }
      );
    }
  });
  }

  OpenD (idServicio_Puntoservicio, descripcionPuntoservicio) {
  	this.idServicio_Puntoservicio = idServicio_Puntoservicio;
  	this.descripcionPuntoservicio = descripcionPuntoservicio;
  	const dialogRef = this.dialog.open(DialogServicio, {
      width: '250px',
      data: {idServicio_Puntoservicio: this.idServicio_Puntoservicio,
      	descripcionPuntoservicio: this.descripcionPuntoservicio}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

}


@Component({
  selector: 'PserviceD',
  templateUrl: 'PserviceD.html',
  styleUrls: ['./editservicios.component.css'],
  providers:[puntoServicioService]
})

export class DialogServicio {


  constructor(
	public _puntoservicioService:puntoServicioService,
    public dialogRef: MatDialogRef<DialogServicio>,
    @Inject(MAT_DIALOG_DATA)  public puntoservicio: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onAddClick() {
    console.log(this.puntoservicio);
  	this._puntoservicioService.creapuntoServicio(this.puntoservicio).subscribe(
			response=>{
				if (!response) {
					swal("Error", "Hubo un problema.", "error");
				}else{
					swal("Exito", response.message, "success");
					this.dialogRef.close();
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					console.log(body.message)
				}
			}
			);
  }

}


