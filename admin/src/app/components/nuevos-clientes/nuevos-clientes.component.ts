import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { NuevosClientesService } from '../../services/nuevos.clientes.service';
import { AdministradorService } from '../../services/administrador.service';
import { NuevoclienteModel } from '../../models/nuevos.cliente.model';
import { GLOBAL } from '../../services/global';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import swal from'sweetalert2';
import * as $ from 'jquery';

export interface DialogData {
  idProspectocita: string;
  nombreProspectocita: string;
  correoProspectocita:string,
  companiaProspectocita:string,
  telefonoProspectocita:string,
  fechaProspectocita:string,
  comentariosProspectocita:string,
  statusProspectocita:string,
}

@Component({
  selector: 'app-nuevos-clientes',
  templateUrl: './nuevos-clientes.component.html',
  styleUrls: ['./nuevos-clientes.component.css'],
  providers:[AdministradorService, NuevosClientesService]
})
export class NuevosClientesComponent implements OnInit {
	public titulo:string;
	public cliente_nuevo:NuevoclienteModel[];
	public url:string;
	public identity;
	public nombreProspectocita;
    public idProspectocita;
    public correoProspectocita;
    public companiaProspectocita;
    public telefonoProspectocita;
    public fechaProspectocita;
    public comentariosProspectocita;
    public statusProspectocita;

 constructor(
  	private _route: ActivatedRoute,
  	private _router: Router,
	private _administradorService:AdministradorService,
	private _clientenuevoService:NuevosClientesService,
	public dialog: MatDialog

		) {
		
		this.titulo = 'Nuevos Clientes';
		this.url = GLOBAL.url;
		this.identity = _administradorService.getIdentity();
		
	}

	public opennuevosclientes(idProspectocita,nombreProspectocita,correoProspectocita,companiaProspectocita,telefonoProspectocita,fechaProspectocita,comentariosProspectocita,statusProspectocita):void{
    this.idProspectocita = idProspectocita;
    this.nombreProspectocita = nombreProspectocita;
    this.correoProspectocita = correoProspectocita;
    this.companiaProspectocita = companiaProspectocita;
    this.telefonoProspectocita = telefonoProspectocita;
    this.fechaProspectocita = fechaProspectocita;
    this.comentariosProspectocita = comentariosProspectocita;
    this.statusProspectocita = statusProspectocita;
    let dialogRef = this.dialog.open(DialogClienteNuevo, {
      width: '400px',
      data:{
        idProspectocita:this.idProspectocita,
        nombreProspectocita:this.nombreProspectocita,
        correoProspectocita:this.correoProspectocita,
        companiaProspectocita:this.companiaProspectocita,
        telefonoProspectocita:this.telefonoProspectocita,
        fechaProspectocita:this.fechaProspectocita,
        comentariosProspectocita:this.comentariosProspectocita,
        statusProspectocita:this.statusProspectocita
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  ngOnInit() {
  	this.getnuevosClientes();
  }

  public eliminarclienteNuevo(idProspectocita){
    this._clientenuevoService.deleteclienteNuevo(idProspectocita).subscribe(response=>{
        if(!response.prospecto){
          this._router.navigate(['/']);
        }
        this.getnuevosClientes();
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
  }

  elimnuevoCliente(idCliente){
    swal({
      title: '¿Seguro?',
      text: "Se desactivará.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, desactiva al cliente.'
  }).then((result) => {
    if (result.value) {
      this._clientenuevoService.deactivateclienteNuevo(idCliente).subscribe(
        response=>{
        if (!response) {
          // this.alertRegister = 'Error al encontrar';
        }else{
          swal(
            'Desactivado',
            'Se ha desactivado el cliente.',
            'success'
          );
          this.getnuevosClientes();
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          // this.alertRegister = body.message;
          console.log(error)
        }
      }
      );
    }
  });
  }

  regrenuevoCliente(idCliente){
    swal({
      title: '¿Seguro?',
      text: "Se activará.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, activa al cliente.'
  }).then((result) => {
    if (result.value) {
      this._clientenuevoService.activateclienteNuevo(idCliente).subscribe(
        response=>{
        if (!response) {
          // this.alertRegister = 'Error al encontrar';
        }else{
          swal(
            'Activado',
            'Se ha activado el cliente.',
            'success'
          );
          this.getnuevosClientes();
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          // this.alertRegister = body.message;
          console.log(error)
        }
      }
      );
    }
  });
  }

	getnuevosClientes(){
	  	this._route.params.forEach((params: Params)=>{
	  		this._clientenuevoService.getclientesNuevos().subscribe(response=>{
	  			if(!response.prospectos){
	  				this._router.navigate(['/']);
	  			}else{
	  				this.cliente_nuevo = response.prospectos;  
            console.log(this.cliente_nuevo);                 
	  			}
	  		},
	  		error => {
	  			var errorMessage = <any>error;
	  			if(errorMessage != null){
	  				var body = JSON.parse(error._body);
	  				console.log(error);
	  			}
	  		}
	  		); 

	  	});
	  }
	}



	@Component({
	  selector: 'app-nuevosClientes-dialog',
	  templateUrl: './popupNuevosClientes.html',
	  styleUrls: ['./nuevos-clientes.component.css']
	})
	export class DialogClienteNuevo {

	  constructor(
	    public dialogRef: MatDialogRef<DialogClienteNuevo>,
	    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

	  onNoClick(): void {
	    this.dialogRef.close();
	  }

	}
