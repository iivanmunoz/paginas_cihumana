import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class ClientesService{
	public url:string;
	public identity;
	public token;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	
	verClientes(){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'obtenerClientes', options)
		.pipe(map(res => res.json()));
	}

	verCliente(idCliente:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'obtenerCliente/'+idCliente, options)
		.pipe(map(res => res.json()));
	}

	añadirCliente(cliente){
		let json = JSON.stringify(cliente);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'creaCliente', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	actualiza(cliente){
		let json = JSON.stringify(cliente);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'actualizaCliente/'+cliente.idCliente, params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	elimina(cliente){
		return this._http.delete(this.url+'eliminaCliente/'+cliente)
		.pipe(map(res => res.json()));
	}

	dabaja(idCliente){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'desactivarCliente/'+idCliente, options)
		.pipe(map(res => res.json()));
	}

	daalta(idCliente){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'activarCliente/'+idCliente, options)
		.pipe(map(res => res.json()));
	}

	getToken(){
		let token = localStorage.getItem('token');
		if (token != 'undefined') {
			this.token = token;
		}else{
			this.token = null;
		}
		return this.token;
	}
}




