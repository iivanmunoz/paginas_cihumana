import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class AspiranteService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	getAspirantes(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'aspirantes', {headers:headers})
		.pipe(map(res => res.json()));	
	}

	getAspirante(idAspirante: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'aspirante/'+idAspirante, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	deleteAspirante(idAspirante: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.delete(this.url+'eliminarAspirante/'+idAspirante, {headers:headers})
		.pipe(map(res => res.json()));
	}
	
	deactivateAspirante(idAspirante: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'desactivarAspirante/'+idAspirante, {headers:headers})
		.pipe(map(res => res.json()));
	}

	activateAspirante(idAspirante: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'activarAspirante/'+idAspirante, {headers:headers})
		.pipe(map(res => res.json()));
	}
}




