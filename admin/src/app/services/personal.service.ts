import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class PersonalService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;

	}

	registerPersonal(personal_to_register){
	 	let json = JSON.stringify(personal_to_register);
		let params = json;
	 	let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'creaPersonal', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}


	getPersonal(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'obtenerPersonal', {headers:headers})
		.pipe(map(res => res.json()));	
	}
     

    eliminaPersonal(personal_to_delete){
     	return this._http.delete(this.url+'eliminaPersonal/'+personal_to_delete)
     	.pipe(map(res => res.json()));	
    }


	vePersonal(idPersonal:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'vePersonal/'+idPersonal, options)
		.pipe(map(res => res.json()));	
	}


	updatePersonal(personal_to_update){
		let json = JSON.stringify(personal_to_update);
		let params = json;
		let headers = new Headers({
			'Content-Type':'application/json'
		});
		return this._http.put(this.url+'actualizaPersonal/'+personal_to_update.idPersonal, params, {headers:headers})
		.pipe(map(res => res.json()));	
	}


	dabaja(idPersonal){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'dabajaPersonal/'+idPersonal, options)
		.pipe(map(res => res.json()));
	}

	daalta(idPersonal){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'daaltaPersonal/'+idPersonal, options)
		.pipe(map(res => res.json()));
	}


}



