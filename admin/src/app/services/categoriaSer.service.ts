import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class CategoriasServService{
	public url:string;
	public identity;
	public token;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	
	verCategoriasSer(){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'getCategoriaSer', options)
		.pipe(map(res => res.json()));
	}

	verCategoriaSer(idCategoriaservicio_Servicio:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'getCategoriaSer/'+idCategoriaservicio_Servicio, options)
		.pipe(map(res => res.json()));
	}

	añadirCategoriaSer(categoriaSer){
		let json = JSON.stringify(categoriaSer);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'creaCategoriaSer', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	actualizaCategoriaSer(categoriaSer){
		let json = JSON.stringify(categoriaSer);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'actualizaCategoriaSer/'+categoriaSer.idCategoriaservicio_Servicio, params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	eliminaCategoriaSer(categoriaSer){
		return this._http.delete(this.url+'eliminaCategoriaSer/'+categoriaSer)
		.pipe(map(res => res.json()));
	}

	dabajaCategoriaSer(idCategoriaservicio_Servicio){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'deactivatecategoriaSer/'+idCategoriaservicio_Servicio, options)
		.pipe(map(res => res.json()));
	}

	daaltaCategoriaSer(idCategoriaservicio_Servicio){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'activatecategoriaSer/'+idCategoriaservicio_Servicio, options)
		.pipe(map(res => res.json()));
	}

	getToken(){
		let token = localStorage.getItem('token');
		if (token != 'undefined') {
			this.token = token;
		}else{
			this.token = null;
		}
		return this.token;
	}
}




