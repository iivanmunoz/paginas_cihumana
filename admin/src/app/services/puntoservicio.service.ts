import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class puntoServicioService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;

	}

	creapuntoServicio(service_to_create){
	 	let json = JSON.stringify(service_to_create);
		let params = json;
	 	let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'crearpuntoServicio', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}


	getpuntoServicios(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'getpuntoServicios', {headers:headers})
		.pipe(map(res => res.json()));	
	}

	vepuntoServicios(idPuntoservicio:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'vepuntoServicios/'+idPuntoservicio, options)
		.pipe(map(res => res.json()));	
	}


	actualizapuntoServicio(servicep_to_update){
		let json = JSON.stringify(servicep_to_update);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'actualizapuntoServicio/'+servicep_to_update.idPuntoservicio, params, {headers:headers})
		.pipe(map(res => res.json()));	
	}


	dabaja(idPuntoservicio){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'dabajapuntoServicio/'+idPuntoservicio, options)
		.pipe(map(res => res.json()));
	}

	daalta(idPuntoservicio){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'daaltapuntoServicio/'+idPuntoservicio, options)
		.pipe(map(res => res.json()));
	}

	buscapuntoServicios(idServicio_Puntoservicio){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'buscapuntoServicios/'+idServicio_Puntoservicio, options)
		.pipe(map(res => res.json()));
	}

}