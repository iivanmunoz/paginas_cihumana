import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class BlogService{
	public url:string;
	public identity;
	public token;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	getBlog(page){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'obtenernoticiaBlog', options)
		.pipe(map(res => res.json()));
	}

	verBlog(idNoticiablog:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'obtenernoticiaidBlog/'+idNoticiablog, options)
		.pipe(map(res => res.json()));
	}

	crea(course_to_create){
		let json = JSON.stringify(course_to_create);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'creanoticiaBlog', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	actualiza(course_to_update){
		let json = JSON.stringify(course_to_update);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'actualizanoticiaBlog/'+course_to_update.idNoticiablog, params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	elimina(course_to_delete){
		return this._http.delete(this.url+'eliminanoticiaBlog/'+course_to_delete)
		.pipe(map(res => res.json()));
	}

	dabaja(idNoticiablog){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'dabajaBlog/'+idNoticiablog, options)
		.pipe(map(res => res.json()));
	}

	daalta(idNoticiablog){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'daaltaBlog/'+idNoticiablog, options)
		.pipe(map(res => res.json()));
	}
}




