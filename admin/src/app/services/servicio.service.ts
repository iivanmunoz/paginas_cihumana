import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class ServicioService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;

	}

	creaServicio(service_to_create){
	 	let json = JSON.stringify(service_to_create);
		let params = json;
	 	let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'crearServicio', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}


	getServicios(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'getServicios', {headers:headers})
		.pipe(map(res => res.json()));	
	}

	veServicio(idServicio:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'veServicio/'+idServicio, options)
		.pipe(map(res => res.json()));	
	}


	updateServicio(service_to_update){
		let json = JSON.stringify(service_to_update);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'actualizaServicio/'+service_to_update.idServicio, params, {headers:headers})
		.pipe(map(res => res.json()));	
	}


	dabaja(idServicio){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'dabajaServicio/'+idServicio, options)
		.pipe(map(res => res.json()));
	}

	daalta(idServicio){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'daaltaServicio/'+idServicio, options)
		.pipe(map(res => res.json()));
	}


}