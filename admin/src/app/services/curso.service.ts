import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class CursoService{
	public url:string;
	public identity;
	public token;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	verCursos(page){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'getCurso', options)
		.pipe(map(res => res.json()));
	}

	verCurso(id:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'veCurso/'+id, options)
		.pipe(map(res => res.json()));
	}

	crea(course_to_create){
		let json = JSON.stringify(course_to_create);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'creaCurso', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	actualiza(course_to_update){
		let json = JSON.stringify(course_to_update);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'actualizaCurso/'+course_to_update.idCurso, params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	elimina(course_to_delete){
		return this._http.delete(this.url+'eliminaCurso/'+course_to_delete)
		.pipe(map(res => res.json()));
	}

	dabaja(idCurso){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'dabajaCurso/'+idCurso, options)
		.pipe(map(res => res.json()));
	}

	daalta(idCurso){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'daaltaCurso/'+idCurso, options)
		.pipe(map(res => res.json()));
	}

	getIdentity(){
		let identity = JSON.parse(localStorage.getItem('identity'));
		if (identity != 'undefined') {
			this.identity = identity;
		}else{
			this.identity = null;
		}
		return this.identity;
	}

	getToken(){
		let token = localStorage.getItem('token');
		if (token != 'undefined') {
			this.token = token;
		}else{
			this.token = null;
		}
		return this.token;
	}
}




