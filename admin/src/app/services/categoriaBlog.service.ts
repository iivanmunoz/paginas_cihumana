import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class CategoriasBlogService{
	public url:string;
	public identity;
	public token;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	
	verCategoriasBlog(){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'getCategoriaBlo', options)
		.pipe(map(res => res.json()));
	}

	verCategoriaBlog(idCategoriablog:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'getCategoriaBlo/'+idCategoriablog, options)
		.pipe(map(res => res.json()));
	}

	añadirCategoriaBlog(categoriablog){
		let json = JSON.stringify(categoriablog);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'creaCategoriaBlo', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	actualizaCategoriaBlog(categoriablog){
		let json = JSON.stringify(categoriablog);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'actualizaCategoriaBlo/'+categoriablog.idCategoriablog, params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	eliminaCategoriaBlog(categoriablog){
		return this._http.delete(this.url+'eliminaCategoriaBlo/'+categoriablog)
		.pipe(map(res => res.json()));
	}

	dabajaCategoriaBlog(idCategoriablog){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'deactivatecategoriaBlog/'+idCategoriablog, options)
		.pipe(map(res => res.json()));
	}

	daaltaCategoriaBlog(idCategoriablog){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.put(this.url+'activatecategoriaBlog/'+idCategoriablog, options)
		.pipe(map(res => res.json()));
	}

	getToken(){
		let token = localStorage.getItem('token');
		if (token != 'undefined') {
			this.token = token;
		}else{
			this.token = null;
		}
		return this.token;
	}
}




