import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class NuevosClientesService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	getclientesNuevos(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'obtenernuevosClientes', {headers:headers})
		.pipe(map(res => res.json()));	
	}

	getclienteNuevo(idProspectocita: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'obtenernuevoCliente/'+idProspectocita, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	deleteclienteNuevo(idProspectocita: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.delete(this.url+'eliminanuevoCliente/'+idProspectocita, {headers:headers})
		.pipe(map(res => res.json()));
	}
	
	deactivateclienteNuevo(idProspectocita: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'dabajanuevoCliente/'+idProspectocita, {headers:headers})
		.pipe(map(res => res.json()));
	}

	activateclienteNuevo(idProspectocita: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'daaltanuevoCliente/'+idProspectocita, {headers:headers})
		.pipe(map(res => res.json()));
	}
}




