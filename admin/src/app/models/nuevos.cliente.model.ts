import { Component, OnInit } from '@angular/core';

export class NuevoclienteModel {

  constructor(
  	public idProspectocita:string,
  	public nombreProspectocita:string,
  	public correoProspectocita:string,
  	public companiaProspectocita:string,
  	public telefonoProspectocita:string,
  	public fechaProspectocita:string,
  	public comentariosProspectocita:string,
  	public statusProspectocita:string
  	) { }

}