import { Component, OnInit } from '@angular/core';

export class ClienteModel {

  constructor(
  	public idCliente:string,
  	public empresaCliente:string,
  	public statusCliente:string,
  	public fotoCliente:string
  	) { }

}