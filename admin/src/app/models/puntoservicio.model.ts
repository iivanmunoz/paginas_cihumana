import { Component, OnInit } from '@angular/core';

export class puntoServicioModel {

  constructor(
  	public idPuntoservicio:string,
  	public descripcionPuntoservicio:string,
  	public idServicio_Puntoservicio:string,
  	public statusPuntoservicio:string
  	) { }

}