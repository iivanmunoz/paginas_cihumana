import { Component, OnInit } from '@angular/core';

export class CategoriasSerModel {

  constructor(
  	public idCategoriaservicio_Servicio:string,
  	public nombreCategoriaservicio:string,
  	public statusCategoriaservicio:string
  	) { }

}