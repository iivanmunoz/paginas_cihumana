import { Component, OnInit } from '@angular/core';

export class AdministradorModel {

  constructor(
  	public idAdministrador:string,
  	public nombreAdministrador:string,
  	public apellidopaternoAdministrador:string,
  	public apellidomaternoAdministrador:string,
  	public correoAdministrador:string,
  	public passwordAdministrador:string,
  	public fotoAdministrador:string
  	) { }

}