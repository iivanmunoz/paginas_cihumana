import { Component, OnInit } from '@angular/core';

export class ServicioModel {

  constructor(
  	public idServicio:string,
  	public idCategoriaservicio_Servicio:string,
  	public nombreServicio:string,
  	public descripcionServicio:string,
  	public urlvideoServicio:string,
  	public statusServicio:string
  	) { }

}