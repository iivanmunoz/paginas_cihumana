import { Component, OnInit } from '@angular/core';

export class CategoriasBlogModel {

  constructor(
  	public idCategoriablog:string,
  	public nombreCategoriablog:string,
  	public statusCategoriablog:string
  	) { }

}