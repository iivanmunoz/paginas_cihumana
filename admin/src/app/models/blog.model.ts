import { Component, OnInit } from '@angular/core';

export class BlogModel {

  constructor(
  	public idNoticiablog:string,
  	public idCategoriablog_Noticiablog:string,
  	public imagenNoticiablog:string,
  	public tituloNoticiablog:string,
  	public descripcionNoticiablog:string,
  	public fechaNoticiablog:Date,
  	public statusNoticiablog:string
  	) { }

}