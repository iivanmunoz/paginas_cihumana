import { Component, OnInit } from '@angular/core';

export class CursoModel {

  constructor(
  	public idCurso:string,
  	public nombreCurso:string,
  	public descripcionCurso:string,
  	public aprendizajeCurso:string,
  	public objetivoCurso:string,
  	public duracionCurso:string,
  	public urlCurso:string,
  	public imagenCurso:string
  	) { }

}