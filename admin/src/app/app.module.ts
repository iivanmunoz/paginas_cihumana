import { BrowserModule } from '@angular/platform-browser';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { routing, appRoutingProviders } from './app.routing';

import { AppComponent } from './app.component';
// Rutas
import { PruebaComponent } from './components/prueba/prueba.component';
import { ActualizaAdministradorComponent } from './components/actualiza-administrador/actualiza-administrador.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';

@NgModule({
  exports: [
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],
  declarations: [NavSideComponent]
})
export class DemoMaterialModule {}


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AspirantesComponent } from './components/aspirantes/aspirantes.component';
import { DialogOverviewExampleDialog} from './components/aspirantes/aspirantes.component';
import { CursosComponent, DialogCurso } from './components/cursos/cursos.component';
import { RegcursoComponent } from './components/cursos/regcurso.component';
import { EditcursoComponent } from './components/cursos/editcurso.component';
import { ClientesComponent, DialogCliente } from './components/clientes/clientes.component';
import { EditClienteComponent } from './components/clientes/editcliente.component';
import { RegclienteComponent } from './components/clientes/regcliente.component';
import { NuevosClientesComponent } from './components/nuevos-clientes/nuevos-clientes.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { BlogComponent } from './components/blog/blog.component';
import { DialogClienteNuevo } from './components/nuevos-clientes/nuevos-clientes.component';
import { CategoriasBlogComponent } from './components/categorias/categoriasBlog.component';
import { CategoriasSerComponent } from './components/categorias/categoriasSer.component';

import { DialogCategoriaBlog } from './components/categorias/categoriasBlog.component';

import { DialogCategoriaSer } from './components/categorias/categoriasSer.component';
import { DialogregistroCategoriaBlog } from './components/categorias/categoriasBlog.component';
import { DialogregistroCategoriaSer } from './components/categorias/categoriasSer.component';
import { NavSideComponent } from './components/nav-side/nav-side.component';

import { PersonalComponent } from './components/personal/personal.component';
import { EditpersonalComponent } from './components/personal/editpersonal.component';
import { RegpersonalComponent } from './components/personal/regpersonal.component';
import { ServiciosComponent } from './components/servicios/servicios.component';
import { RegserviciosComponent } from './components/servicios/regservicios.component';
import { EditserviciosComponent } from './components/servicios/editservicios.component';
import { DialogServicio } from './components/servicios/editservicios.component';
import { RegblogComponent } from './components/blog/regblog.component';
import { EditblogComponent } from './components/blog/editblog.component';







@NgModule({
  declarations: [
    AppComponent,
    PruebaComponent,
    LoginComponent,
    RegisterComponent,
    ActualizaAdministradorComponent,
    NavbarComponent,
    AspirantesComponent,
    DialogOverviewExampleDialog,
    CursosComponent,
    EditcursoComponent,
    DialogCurso,
    RegcursoComponent,
    DialogCliente,
    ClientesComponent,
    EditClienteComponent,
    RegclienteComponent,
    NuevosClientesComponent,
    CategoriasComponent, 
    BlogComponent,
    DialogClienteNuevo,
    CategoriasBlogComponent,
    CategoriasSerComponent,
    
    DialogCategoriaBlog,
    DialogCategoriaSer,
    DialogregistroCategoriaBlog,
    DialogregistroCategoriaSer,
    PersonalComponent,
    EditpersonalComponent,
    RegpersonalComponent,
    ServiciosComponent,
    RegserviciosComponent,
    EditserviciosComponent,
    DialogServicio,
    RegblogComponent,
    EditblogComponent,
    // DialogeditCategoriaBlog
  ],
  entryComponents:[
  DialogOverviewExampleDialog,
  DialogClienteNuevo,
  DialogCategoriaBlog,
  DialogCategoriaSer,
  DialogregistroCategoriaBlog,
  DialogregistroCategoriaSer,
  DialogServicio,
  // DialogeditCategoriaBlog
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    BrowserAnimationsModule,
    DemoMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule
  ],
  providers: [
  appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
