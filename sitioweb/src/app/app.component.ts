import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ProspectoService } from './services/contacto.service';
import { ProspectoModel } from './models/contacto.model';
import { GLOBAL } from './services/global';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import * as $ from 'jquery';

export interface DialogData {

  idProspectocita: string,
  nombreProspectocita: string,
  correoProspectocita:string,
  companiaProspectocita:string,
  telefonoProspectocita:string,
  fechaProspectocita:string,
  comentariosProspectocita:string

}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ProspectoService]
})
export class AppComponent {
    public prospecto:ProspectoModel;
	public alertRegister;
	public finurl: string;

	public idProspectocita: string;
	public nombreProspectocita: string;
	public correoProspectocita:string;
	public companiaProspectocita:string;
	public telefonoProspectocita:string;
	public fechaProspectocita:string;
	public comentariosProspectocita:string;

    constructor(
    	private _route: ActivatedRoute,
	  	private _router: Router,
	  	private _prospectoservice:ProspectoService,
	  	public dialog: MatDialog) {
		this.prospecto = new ProspectoModel('','','','','','','');
	}

	public openContacto(){
 

    

    let dialogRef = this.dialog.open(DialogContacto, {
      width: '400px',
      data:{
        idProspectocita:this.idProspectocita,
        nombreProspectocita:this.nombreProspectocita,
        correoProspectocita:this.correoProspectocita,
        companiaProspectocita:this.companiaProspectocita,
        telefonoProspectocita:this.telefonoProspectocita,
        fechaProspectocita:this.fechaProspectocita,
        comentariosProspectocita:this.comentariosProspectocita
      }

    });
    dialogRef.afterOpen().subscribe(result => {
      this.hideAgenda();
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
      this.showAgenda();
    });
  }

  public showAgenda(){
    document.getElementById('floato').style.display = 'block';
  }
  public hideAgenda(){
    document.getElementById('floato').style.display = 'none';
  }

    ngOnInit() { }
}

@Component({
  selector: 'app-servicios-dialog',
  templateUrl: './dialogcita.html',
  styleUrls: ['./app.component.css'],
  providers: [ProspectoService]
})
export class DialogContacto {
	public prospecto:ProspectoModel;
	public alertRegister;

  constructor(
     private _route: ActivatedRoute,
      private _router: Router,
    private _prospectoservice:ProspectoService,
    public dialogRef: MatDialogRef<DialogContacto>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  	this.prospecto = new ProspectoModel('','','','','','','');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
      }
    

     public onSubmitRegister(){
      if(this.validar() == true){
        this._prospectoservice.register(this.prospecto).subscribe(
      response=>{
        let prosp = response.idProspectocita;
        this.prospecto = prosp;
        if (!prosp) {
          this.alertRegister = 'Error al enviar';
          this.prospecto = new ProspectoModel('','','','','','','');
        }else{
          this.alertRegister = 'Formulario enviado con exito';
          this.prospecto = new ProspectoModel('','','','','','','');
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          // this.alertRegister = body.message;
          // console.log(error)
        }
      }
      );

      }
    
  }

  public validar() {
    if ($('#nombre').val() == "") {
      this.alertRegister = 'Ingrese nombre';
      return false;
    }
    else if ($('#telefono, #telefono2').val() == "") {
      this.alertRegister = 'Ingrese telefono';
      return false;
    }
    else if ($('#email,#email2').val() == "") {
      this.alertRegister = 'Ingrese email';
      return false;
    }
    else{
      return true;
    }
    

  }

}
