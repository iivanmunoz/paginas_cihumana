import { Component, OnInit } from '@angular/core';

export class ClienteServicio {

  constructor(
  	public idClienteServicio:string,
  	public idCliente_ClienteServicio:string,
  	public idServicio_ClienteServicio:string,
  	public descripcionServicio_ClienteServicio:string
  	) { }

}