import { Component, OnInit } from '@angular/core';

export class PersonalModel {

  constructor(
  	public idPersonal:string,
  	public nombrePersonal:string,
  	public apellidopaternoPersonal:string,
  	public apellidomaternoPersonal:string,
  	public telefonoPersonal:string,
  	public puestoPersonal:string,
  	public sueldoPersonal:string,
  	public statusPersonal:string,
    public frasePersonal:string,
    public fotoPersonal:string
  	) { }

}