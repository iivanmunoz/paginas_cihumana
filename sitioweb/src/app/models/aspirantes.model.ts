import { Component, OnInit } from '@angular/core';

export class AspiranteModel{

  constructor(
  	public idAspirante:string,
  	public nombreAspirante:string,
  	public apellidopaternoAspirante:string,
  	public apellidomaternoAspirante:string,
  	public correoAspirante:string,
  	public telefonoAspirante:string,
  	public cvAspirante:string,
  	public ocupacionAspirante:string,
  	public statusAspirante:string,
  	public fechaAspirante:string
  	) { }

}