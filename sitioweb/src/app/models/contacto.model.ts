import { Component, OnInit } from '@angular/core';

export class ProspectoModel{

  constructor(
  	public idProspectocita:string,
  	public nombreProspectocita:string,
  	public correoProspectocita:string,
  	public companiaProspectocita:string,
  	public telefonoProspectocita:string,
  	public fechaProspectocita:string,
  	public comentariosProspectocita:string
  	) { }

}