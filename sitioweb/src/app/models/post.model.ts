import { Component, OnInit } from '@angular/core';

export class PostModel {

  constructor(
  	public idNoticiablog:string,
  	public idCategoriablog_Noticiablog:string,
  	public imagenNoticiablog:string,
  	public tituloNoticiablog:string,
  	public descripcionNoticiablog:string,
  	public fechaNoticiablog:string,
  	public statusNoticiablog:string
  	) { }

}