import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ProspectoService } from '../../services/contacto.service';
import { ProspectoModel } from '../../models/contacto.model';
import { GLOBAL } from '../../services/global';
import * as $ from 'jquery';

@Component({
    selector: 'app-contacto',
    templateUrl: './contacto.component.html',
    styleUrls: ['./contacto.css'],
    providers: [ProspectoService]
})

export class ContactoComponent implements OnInit {
    public prospecto:ProspectoModel;
	public alertRegister;
	public finurl: string;

    constructor(
		private _route: ActivatedRoute,
	  	private _router: Router,
    	private _prospectoservice:ProspectoService) {
    	this.finurl = _router.url;
		this.prospecto = new ProspectoModel('','','','','','','');
	}

    ngOnInit() { 
    	if (this.finurl!='/contacto') {
       		
     	} else{
     		document.getElementById('floato').style.display = 'none';
     		document.getElementById('floato2').style.display = 'none';
     	}
    }

    public onSubmitRegister(){
    	
    		this._prospectoservice.register(this.prospecto).subscribe(
			response=>{
				let prosp = response.idProspectocita;
				this.prospecto = prosp;
				if (!prosp) {
					this.alertRegister = 'Error al enviar';
					this.prospecto = new ProspectoModel('','','','','','','');
				}else{
					this.alertRegister = 'Formulario enviado con exito';
					this.prospecto = new ProspectoModel('','','','','','','');
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					// this.alertRegister = body.message;
					// console.log(error)
				}
			}
			);

    	
		
	}

	
	
}
