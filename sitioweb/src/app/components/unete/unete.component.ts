import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { AspiranteModel } from '../../models/aspirantes.model';
import { AspiranteService } from '../../services/aspirantes';
import { GLOBAL } from '../../services/global';
// import swal from'sweetalert2';
import * as $ from 'jquery';

@Component({
    selector: 'app-unete',
    templateUrl: './unete.component.html',
    styleUrls: ['./unete.css',],
    providers: [AspiranteService]
})

export class UneteComponent implements OnInit {
    
    public aspirante:AspiranteModel;
	public alertRegister;
	public filesToUpload: Array<File>;
	public token;
	public url:string;
	public finurl:string;


	constructor(
		private _route: ActivatedRoute,
	  	private _router: Router,
	  	private _aspiranteService:AspiranteService) {
		this.aspirante = new AspiranteModel('','','','','','','','','','');
		this.url = GLOBAL.url;
		this.finurl = _router.url;
	}

	ngOnInit() {
		if (this.finurl!='/unete') {
       		
     	} else{
     		document.getElementById('floato').style.display = 'none';
     		document.getElementById('floato2').style.display = 'none';
     	}
	}

	public onSubmitRegister(){
		if(this.validar() == true){
		this._aspiranteService.register(this.aspirante).subscribe(
			response=>{
				let aspirantee = response.idAspirante;
				console.log(aspirantee);
				this.aspirante = aspirantee;
				console.log(this.aspirante);
				if (!aspirantee) {
					this.alertRegister = 'Error al registrarse';
					console.log('error')
				}else{
					if (!this.filesToUpload) {
						console.log('aoida');
						if (this.aspirante.cvAspirante != 'default.doc') {
							this.aspirante.cvAspirante = this.aspirante.cvAspirante;
						}
						localStorage.setItem('identity', JSON.stringify(this.aspirante));
					}else{
						console.log('aoida3');
						this.makeFileRquest(this.url+'subecv/'+this.aspirante,
							[], this.filesToUpload).then(
							(result:any)=>{
								this.aspirante.cvAspirante = result.cvAspirante.cvAspirante;								
								localStorage.setItem('identity', JSON.stringify(this.aspirante));
								let urlImagen = this.url+'obtenercv/'+this.aspirante.cvAspirante;
							}
							).catch(error=>{
								console.log(`${error}`);
							})
						}

						 this.alertRegister = 'Se ha enviado correctamente';
						// swal("Exito", response.message, "success");
					}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					this.alertRegister = body.message;
					console.log(error)
				}
			}
			);
		}
	}


	fileChangeEvent(fileInput:any){
		this.filesToUpload = <Array<File>>fileInput.target.files;
		$('#info').text(this.filesToUpload[0].name);
	}

	makeFileRquest(url:string,params:Array<string>, files:Array<File>){
		let token = this.token;
		return new Promise(function(resolve,reject){
			var formData:any = new FormData();
			var xhr = new XMLHttpRequest();
			for (var i = 0; i < files.length; i++) {
				formData.append('cvAspirante',files[i],files[i].name)
			}
			xhr.onreadystatechange = function(){
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
						resolve(JSON.parse(xhr.response));	
					}else{
						reject(xhr.response);
					}
				}
			}
			xhr.open('PUT',url,true);
			xhr.setRequestHeader('Authorization',token);
			xhr.send(formData);
		})
	}

	public validar() {
	  if ($('#nombreAspirante').val() == "") {
	    this.alertRegister = 'Ingrese nombre';
	    return false;
	  }
	  else if ($('#telefono').val() == "") {
	    this.alertRegister = 'Ingrese telefono';
	    return false;
	  }
	  else if ($('#email').val() == "") {
	    this.alertRegister = 'Ingrese email';
	    return false;
	  }
	  else if ($('#files').val() == "") {
	    this.alertRegister = 'Ingrese curriculum';
	    return false;
	  }
	  else{
	  	return true;
	  }
	  

	}

}