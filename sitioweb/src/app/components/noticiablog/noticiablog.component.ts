import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { BlogService } from '../../services/blog.service';
import { CategoriaService } from '../../services/categorias.service';
import { BlogModel } from '../../models/blog.model';
import { CategoriasModel } from '../../models/categorias.model';
import { GLOBAL } from '../../services/global';
import * as $ from 'jquery';

@Component({
  selector: 'app-noticiablog',
  templateUrl: './noticiablog.component.html',
  styleUrls: ['./noticiablog.component.css'],
  providers: [BlogService,CategoriaService]
})
export class NoticiablogComponent implements OnInit {
public Noticia:BlogModel;
  public CatBlog:CategoriasModel[];
  public url:string;
  public datere:string;
  public locurl: string;
  public win:Window;
  public finurl:string;
    constructor(
    private _route: ActivatedRoute,
  	private _router: Router,
  	private _blogService:BlogService,
    private _categoriaService:CategoriaService) { 
      
        this.url = GLOBAL.url;
        this.locurl = window.location.href;
        this.win = window;
        this.finurl = _router.url;
    }

    ngOnInit() { 
      if (this.finurl!='/unete') {
         document.getElementById('floato').style.display = 'none';
         document.getElementById('floato2').style.display = 'none';
         this.getNoticia();  
       } else{
                
       }
    	
    	
    }

    public navigate(){
      this._router.navigate(['/blog']);
    }

     public getNoticia() {
  	this._route.params.forEach((params: Params) =>{
  		let idNoticiablog = params['idNoticiablog'];
  		this._blogService.getNoticiaBlog(idNoticiablog).subscribe(
  			response=>{
				if (!response) {
					
				}else{
					this.Noticia = response.Noticias[0];
				}
			},error=>{
				var errorMensaje = <any>error;
				if (errorMensaje != null) {
					var body = JSON.parse(error._body);
					console.log(error)
				}
			}
			);
  	});
  }

  
}
