import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticiablogComponent } from './noticiablog.component';

describe('NoticiablogComponent', () => {
  let component: NoticiablogComponent;
  let fixture: ComponentFixture<NoticiablogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoticiablogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticiablogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
