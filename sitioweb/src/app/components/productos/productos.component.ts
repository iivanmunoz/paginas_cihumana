import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-productos',
    templateUrl: './productos.component.html',
    styleUrls: ['./productos.css',]
})

export class ProductosComponent implements OnInit {
  public finurl: string;

    constructor(private _route: ActivatedRoute,
  	private _router: Router) { 
    	this.finurl = _router.url;
  	}

    ngOnInit() { 
    if (this.finurl=='/productos') {
      document.getElementById('floato').style.display = 'block';
      document.getElementById('floato2').style.display = 'block';
       
     } 
 }
}
