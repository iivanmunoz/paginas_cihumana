import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { CursoService } from '../../services/cursos.service';
import { CursoModel } from '../../models/cursos.model';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'app-eduacion',
    templateUrl: './educacion.component.html',
    styleUrls: ['./educacion.css'],
    providers:[CursoService]
})

export class EducacionComponent implements OnInit {

    public cursos:CursoModel;
    public curso:CursoModel;
    public url:string;
    public next_page;
    public prev_page;
    public finurl: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        public _cursoService:CursoService
        ) {
        this.url = GLOBAL.url;
        this.next_page = 1;
        this.prev_page =1;
        this.finurl = _router.url;
        
    }

    ngOnInit() {
      if (this.finurl=='/educacion') {
      document.getElementById('floato').style.display = 'block';
      document.getElementById('floato2').style.display = 'block';
       this.getCursos();
     } 
                   
    }

    getCursos() {
      this._route.params.forEach((params:Params) =>{
          let page = +params['page'];
          if (!page) {
              page = 1;
          }else{
              this.next_page = page + 1;
              this.prev_page = page - 1;

              if (this.prev_page ==0) {
                  this.prev_page = 1;
              }
          }
          this._cursoService.verCursos(page).subscribe(
              response=>{
                this.getCurso(response.curso[0].idCurso);
                if (!response) {                    
                }else{                                  
                    this.cursos = response.curso

                }
            },error=>{
                var errorMensaje = <any>error;
                if (errorMensaje != null) {
                    var body = JSON.parse(error._body);
                    console.log(error)
                }
            }
          );
      });
  }

  public getCurso(idCurso) {
    this._route.params.forEach((params: Params) =>{
      this._cursoService.verCurso(idCurso).subscribe(
        response=>{
        if (!response) {
        }else{
          this.curso = response.curso[0];
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          // console.log(error)
        }
      }
      );
    });
  }
            
}
