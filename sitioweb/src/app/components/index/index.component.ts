import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./general.css']
})

export class HomeComponent implements OnInit {
	public finurl: string;
    constructor(private _route: ActivatedRoute,
  	private _router: Router) { 
    	this.finurl = _router.url;
  	}

    ngOnInit() {
    	if (this.finurl=='/' || this.finurl=='/index') {
            document.getElementById('floato').style.display = 'none';
             document.getElementById('floato2').style.display = 'none';
       		
     	} else{
     		
     	}
    }
}
