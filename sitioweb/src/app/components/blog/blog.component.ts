import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { BlogService } from '../../services/blog.service';
import { CategoriaService } from '../../services/categorias.service';
import { BlogModel } from '../../models/blog.model';
import { CategoriasModel } from '../../models/categorias.model';
import { GLOBAL } from '../../services/global';
import * as $ from 'jquery';

@Component({
    selector: 'app-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.css'],
    providers: [BlogService,CategoriaService]
})

export class BlogComponent implements OnInit {
	public Noticias:BlogModel[];
  public Noticia:BlogModel[];
  public CatBlog:CategoriasModel[];
  public url:string;
  public datere:string;
  public locurl: string;
  public finurl: string;
  public win:Window;
  public array = [];
    constructor(
    private _route: ActivatedRoute,
  	private _router: Router,
  	private _blogService:BlogService,
    private _categoriaService:CategoriaService) { 

        this.url = GLOBAL.url;
        this.locurl = window.location.href;
        this.win = window;
        this.finurl = _router.url;
      // this.noticia = new BlogModel('','','','','','','');
    }

    ngOnInit() {      
    if (this.finurl=='/blog') {
      document.getElementById('floato').style.display = 'none';
      document.getElementById('floato2').style.display = 'none';
      this.getNoticias();      
      this.getCategorias();
     } 
    else{
      this.getNoticia();
      this.getCategorias();
    }
      
      
    }

    getNoticias(){
  	this._route.params.forEach((params: Params)=>{
  		this._blogService.getNoticias().subscribe(response=>{
  			if(!response.Noticias){
  				this._router.navigate(['/']);
  			}else{
  				this.Noticias = response.Noticias;
          // console.log(this.Noticias);
           for (var x = 0; x < this.Noticias.length; x++) {
           let date = new Date(this.Noticias[x].fechaNoticiablog);
           //1 test        
           var month = new Array();
              month[0] = "Enero";
              month[1] = "Febrero";
              month[2] = "Marzo";
              month[3] = "Abril";
              month[4] = "Mayo";
              month[5] = "Junio";
              month[6] = "Julio";
              month[7] = "Agosto";
              month[8] = "Septiembre";
              month[9] = "Octubre";
              month[10] = "Noviembre";
              month[11] = "Diciembre";
              var n = month[date.getMonth()];                   
           //end test 
          // console.log(date.getFullYear() + "/" + date.getMonth() + "/" + date.getDay());          
             this.datere = n + " " + date.getDate() + "," + " " + date.getFullYear();
             // console.log(this.datere);
             this.array[x] = this.datere;
             this.Noticias[x].fechaNoticiablog = this.array[x];
             // console.log(this.array);
          }
            
  			  }
  		},
  		error => {
  			var errorMessage = <any>error;
  			if(errorMessage != null){
  				var body = JSON.parse(error._body);
  				console.log(error);
  			}
  		}
  		); 

  	});
  }

  

  filtrarCat(idCategoria){
    if(idCategoria == undefined){
      
    }else{
      this._router.navigate(['/blog/'+idCategoria]);  
    }
    
  }
   getCategorias(){
    this._route.params.forEach((params: Params)=>{
      this._categoriaService.getCategorias().subscribe(response=>{
        if(!response.CatBlog){
          this._router.navigate(['/']);
        }else{
          this.CatBlog = response.CatBlog;
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      );

    });
  }

  public getNoticia() {
    this._route.params.forEach((params: Params) =>{
      let idCategoriablog_Noticiablog = params['idCategoriablog_Noticiablog'];
      this._blogService.getNoticia(idCategoriablog_Noticiablog).subscribe(
        response=>{
        if (!response) {
          
        }else{
          this.Noticia = response.Noticias;
          for (var x = 0; x < this.Noticia.length; x++) {
           let date = new Date(this.Noticia[x].fechaNoticiablog);
           //1 test        
           var month = new Array();
              month[0] = "Enero";
              month[1] = "Febrero";
              month[2] = "Marzo";
              month[3] = "Abril";
              month[4] = "Mayo";
              month[5] = "Junio";
              month[6] = "Julio";
              month[7] = "Agosto";
              month[8] = "Septiembre";
              month[9] = "Octubre";
              month[10] = "Noviembre";
              month[11] = "Diciembre";
              var n = month[date.getMonth()];                   
           //end test 
          // console.log(date.getFullYear() + "/" + date.getMonth() + "/" + date.getDay());          
             this.datere = n + " " + date.getDate() + "," + " " + date.getFullYear();
             // console.log(this.datere);
             this.array[x] = this.datere;
             this.Noticia[x].fechaNoticiablog = this.array[x];
             // console.log(this.array);
          }
        }
      },error=>{
        var errorMensaje = <any>error;
        if (errorMensaje != null) {
          var body = JSON.parse(error._body);
          console.log(error)
        }
      }
      );
    });
  }

  getNots(idCategoriablog_Noticiablog){
    this._route.params.forEach((params: Params)=>{
      this._blogService.getNoticia(idCategoriablog_Noticiablog).subscribe(response=>{
        if(!response.Noticias){
          this._router.navigate(['/']);
        }else{
          this.Noticias = response.Noticias;
          // console.log(this.Noticias);
            for (var x = 0; x < this.Noticias.length; x++) {
           let date = new Date(this.Noticias[x].fechaNoticiablog);
           //1 test        
           var month = new Array();
              month[0] = "Enero";
              month[1] = "Febrero";
              month[2] = "Marzo";
              month[3] = "Abril";
              month[4] = "Mayo";
              month[5] = "Junio";
              month[6] = "Julio";
              month[7] = "Agosto";
              month[8] = "Septiembre";
              month[9] = "Octubre";
              month[10] = "Noviembre";
              month[11] = "Diciembre";
              var n = month[date.getMonth()];                   
           //end test 
          // console.log(date.getFullYear() + "/" + date.getMonth() + "/" + date.getDay());          
             this.datere = n + " " + date.getDate() + "," + " " + date.getFullYear();
             // console.log(this.datere);
             this.array[x] = this.datere;
             this.Noticias[x].fechaNoticiablog = this.array[x];
             // console.log(this.array);
          }

                  }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 

    });
  }

  getCategoria(idCategoriablog){
    this._route.params.forEach((params: Params)=>{
      this._categoriaService.getCategoria(idCategoriablog).subscribe(response=>{
        if(!response.CatBlog){
          this._router.navigate(['/']);
        }else{
          this.CatBlog = response.CatBlog;
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 

    });
  }
}

