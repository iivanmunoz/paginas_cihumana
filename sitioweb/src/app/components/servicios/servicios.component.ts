import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ServiciosService } from '../../services/servicios.service';
import { PuntosserviciosService } from '../../services/puntosservicios.service';
import { ServicioModel } from '../../models/servicios.model';
import { puntoServicioModel } from '../../models/puntosservicios.model';
import { DomSanitizer, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

import { GLOBAL } from '../../services/global';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import swal from 'sweetalert2';
import * as $ from 'jquery';
export interface DialogData {

  idServicio: string;
  idCategoriaservicio_Servicio: string;
  nombreServicio:string,
  descripcionServicio:string,
  urlvideoServicio:string,
  statusServicio:string,
  idPuntoservicio:string,
  descripcionPuntoservicio:string,
  idServicio_Puntoservicio:string,
  statusPuntoservicio:string

}

@Component({
    selector: 'app-servicios',
    templateUrl: './servicios.component.html',
    styleUrls: ['./servicios.css', ],
    providers: [ServiciosService, PuntosserviciosService]
})

export class ServiciosComponent implements OnInit {
    public titulo:string;
    public servicio:ServicioModel[];
    public puntoservicio:puntoServicioModel[];
    public puntoservicio2:puntoServicioModel[];
    public url:string;
    public idServicio: string;
    public idCategoriaservicio_Servicio: string;
    public nombreServicio:string;
    public descripcionServicio:string;
    public urlvideoServicio:SafeResourceUrl;
    public urlvideoServicio2:string;
    public statusServicio:string;

    public idPuntoservicio:string;
    public descripcionPuntoservicio:string;
    public idServicio_Puntoservicio:string;
    public statusPuntoservicio:string;
    public finurl: string;


     constructor(
      private _route: ActivatedRoute,
      private _router: Router,
    private _serviciosService:ServiciosService,
    private _puntosserviciosService:PuntosserviciosService,
    private _sanitizationService: DomSanitizer,
    public dialog: MatDialog

        ) {
        
        this.titulo = 'Servicios';
        this.url = GLOBAL.url;        
        this.finurl = _router.url;

        
    }

    public openServicio(idServicio,idCategoriaservicio_Servicio,nombreServicio,descripcionServicio,urlvideoServicio,statusServicio):void{
    this.idServicio = idServicio;
    this.idCategoriaservicio_Servicio = idCategoriaservicio_Servicio;
    this.nombreServicio = nombreServicio;
    this.descripcionServicio = descripcionServicio;
    this.urlvideoServicio = urlvideoServicio;
    this.statusServicio = statusServicio;
    this.urlvideoServicio = this._sanitizationService.bypassSecurityTrustResourceUrl(urlvideoServicio); 
    let dialogRef = this.dialog.open(DialogServicio, {
      width: '400px',
      data:{
        idServicio:this.idServicio,
        idCategoriaservicio_Servicio:this.idCategoriaservicio_Servicio,
        nombreServicio:this.nombreServicio,
        descripcionServicio:this.descripcionServicio,
        urlvideoServicio:this.urlvideoServicio,
        statusServicio:this.statusServicio   
      }

    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }
    
    ngOnInit() {
      if (this.finurl=='/servicios') {
      document.getElementById('floato').style.display = 'block';
      document.getElementById('floato2').style.display = 'block';
       this.getServicios();
      this.getpuntosServicios();
     } 
      
      
      }

      getServicios(){
    this._route.params.forEach((params: Params)=>{
      this._serviciosService.getServicios().subscribe(response=>{
        if(!response.Servicio){
          this._router.navigate(['/']);
        }else{          
          this.servicio = response.Servicio;      
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 

    });
  }

  getpuntosServicios(){
    this._route.params.forEach((params: Params)=>{
      this._puntosserviciosService.getpuntoServicios().subscribe(response=>{
        if(!response.PuntosdeServicio){
          this._router.navigate(['/']);
        }else{
          this.puntoservicio = response.PuntosdeServicio;        
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 

    });
  }

  public buscapuntoServicios(idServicio) {
    this._route.params.forEach((params: Params)=>{      
      this._puntosserviciosService.buscapuntoServicios(idServicio).subscribe(response=>{
        if(!response){
          console.log('No hay respuesta');
        }else{
          this.puntoservicio = response;
          this.puntoservicio2 = response.PuntosdeServicio;
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
    });
  }


}

@Component({
  selector: 'app-servicios-dialog',
  templateUrl: './dialogservicio.html',
  styleUrls: ['./servicios.css'],
  providers: [PuntosserviciosService]
})
export class DialogServicio {
  public puntoservicio:puntoServicioModel[];
   public puntoservicio2:puntoServicioModel[];

  constructor(
     private _route: ActivatedRoute,
      private _router: Router,
    private _puntosserviciosService:PuntosserviciosService,
    public dialogRef: MatDialogRef<DialogServicio>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
      this.buscapuntoServicios();
      }
    

    public buscapuntoServicios() {
    this._route.params.forEach((params: Params)=>{      
      this._puntosserviciosService.buscapuntoServicios(this.data.idServicio).subscribe(response=>{
        if(!response){
          console.log('No hay respuesta');
        }else{
          this.puntoservicio = response;
          this.puntoservicio2 = response.PuntosdeServicio;
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      ); 
    });
  }

}