import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { BlogService } from '../../services/blog.service';
import { CategoriaService } from '../../services/categorias.service';
import { BlogModel } from '../../models/blog.model';
import { CategoriasModel } from '../../models/categorias.model';
import { GLOBAL } from '../../services/global';
import * as $ from 'jquery';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.css'],
    providers: [BlogService, CategoriaService]
})

export class MenuComponent implements OnInit {
    public Noticias:BlogModel[];
  public CatBlog:CategoriasModel[];
  public url:string;
    constructor(
        private _route: ActivatedRoute,
      private _router: Router,
      private _blogService:BlogService,
    private _categoriaService:CategoriaService) { }

    ngOnInit(
        ) {
        this.getCategorias(); 
    }

    openNav() {
        document.getElementById('mySidenav').style.width = '100%';
    }

    closeNav() {
        document.getElementById('mySidenav').style.width = '0';
    }

    openCat(){
        document.getElementById('est').style.display = 'block';
    }

    closeCat(){
        document.getElementById('est').style.display = 'none';
    }

    getCategorias(){
    this._route.params.forEach((params: Params)=>{
      this._categoriaService.getCategorias().subscribe(response=>{
        if(!response.CatBlog){
          this._router.navigate(['/']);
        }else{
          this.CatBlog = response.CatBlog;
        }
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          var body = JSON.parse(error._body);
          console.log(error);
        }
      }
      );

    });
  }

}
