import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { ClientesService } from '../../services/clientes.service';
import { ClienteModel } from '../../models/clientes.model';
import { ClienteServicio } from '../../models/clienteservicio.model';
import { PersonalModel } from '../../models/personal.model';
import { PersonalService } from '../../services/personal.service';
import { GLOBAL } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatInputModule, MatFormFieldModule} from '@angular/material';

export interface DialogData {
  idCliente: string;
  empresaCliente:string,
  telefonoCliente:string,
  statusCliente:string,
  fotoCliente:string,
  url:string
}

export interface DialogData2 {
  url:string
}

@Component({
    selector: 'app-nosotros',
    templateUrl: './nosotros.component.html',
    styleUrls: ['./nosotros.css', '../agenda/agenda.css'],
    providers: [ClientesService]
})

export class NosotrosComponent implements OnInit {
    
    public titulo:string;
    public client:ClienteModel;
    public clients:ClienteModel;

    public identity;
    public token;
    public alertRegister;
    public url:string;
    public idCliente;
    public empresaCliente;
    public telefonoCliente;
    public statusCliente;
    public fotoCliente;
    public idPersonal;
    public nombrePersonal;
    public apellidopaternoPersonal;
    public apellidomaternoPersonal;
    public telefonoPersonal;
    public puestoPersonal;
    public sueldoPersonal;
    public statusPersonal;
    public frasePersonal;
    public fotoPersonal;
  public finurl: string;

 constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        public _clienteService:ClientesService,
        public dialog: MatDialog
        ) {
        this.titulo = 'Cursos';
        this.url = GLOBAL.url;
        this.finurl = _router.url;
        
    }

    public openCliente(idCliente,empresaCliente,telefonoCliente,statusCliente,fotoCliente,url):void{
    this.idCliente = idCliente;
    this.empresaCliente = empresaCliente;
    this.telefonoCliente = telefonoCliente;
    this.statusCliente = statusCliente;
    this.fotoCliente = fotoCliente;
    this.url = url;
    let dialogRef = this.dialog.open(DialogCliente, {
      width: '300px',
      height: 'auto',
      data:{
        idCliente:this.idCliente,
        empresaCliente:this.empresaCliente,
        telefonoCliente:this.telefonoCliente,
        statusCliente:this.statusCliente,
        fotoCliente:this.fotoCliente,
        url: this.url
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  public openPersonal(url):void{
    this.url = url;
    let dialogRef = this.dialog.open(DialogPersonal, {
      width: '600px',
      height: '5px',
      data:{
        url: this.url
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }


  ngOnInit() {
      if (this.finurl=='/nosotros') {
      document.getElementById('floato').style.display = 'block';
      document.getElementById('floato2').style.display = 'block';
       this.getClientes();
     } 
      
      
  }

  getCliente() {
      this._route.params.forEach((params: Params) =>{
          let idCliente = params['idCliente'];
          this._clienteService.verCliente(idCliente).subscribe(
              response=>{
                if (!response) {
                    
                }else{
                    this.client = response.client
                }
            },error=>{
                var errorMensaje = <any>error;
                if (errorMensaje != null) {
                    var body = JSON.parse(error._body);
                    console.log(error)
                }
            }
            );
      });
  }

  getClientes() {
      this._route.params.forEach((params:Params) =>{
          this._clienteService.verClientes().subscribe(
              response=>{
                if (!response) {
                    
                }else{
                    this.clients = response.client
                }
            },error=>{
                var errorMensaje = <any>error;
                if (errorMensaje != null) {
                    var body = JSON.parse(error._body);
                    this.alertRegister = body.message;
                    console.log(error)
                }
            }
          );
      });
  }


}


@Component({
  selector: 'cliente-dialog',
  templateUrl: 'cliente-dialog.html',
  styleUrls: ['../clientes/clientes.css'],
  providers:[ClientesService]
})

export class DialogCliente {

  public clientsserv:ClienteServicio;

  constructor(
      public _clienteService:ClientesService,
      private _route: ActivatedRoute,
        private _router: Router,
    public dialogRef: MatDialogRef<DialogCliente>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}


  ngOnInit() {

      this.getClientesServ();
      
  }


  onNoClick(): void {
    this.dialogRef.close();
  }


  getClientesServ() {
      this._route.params.forEach((params:Params) =>{
          this._clienteService.veClienteServ().subscribe(
              response=>{
                if (!response) {
                    
                }else{
                    this.clientsserv = response.servclient
                }
            },error=>{
                var errorMensaje = <any>error;
                if (errorMensaje != null) {
                    var body = JSON.parse(error._body);
                    console.log(error)
                }
            }
          );
      });
  }
}

@Component({
  selector: 'personal-dialog',
  templateUrl: 'personal-dialog.html',
  styleUrls: ['./personal.css'],
  providers:[PersonalService]
})

export class DialogPersonal {

  public personal:PersonalModel;

  constructor(
      public _personalService:PersonalService,
      private _route: ActivatedRoute,
        private _router: Router,
    public dialogRef: MatDialogRef<DialogPersonal>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData2) {}


  ngOnInit() {

      this.getPersonal();
      
  }


  onNoClick(): void {
    this.dialogRef.close();
  }


  getPersonal() {
      this._route.params.forEach((params:Params) =>{
          this._personalService.getPersonal().subscribe(
              response=>{
                if (!response) {
                    
                }else{
                    this.personal = response.personal
                }
            },error=>{
                var errorMensaje = <any>error;
                if (errorMensaje != null) {
                    var body = JSON.parse(error._body);
                    console.log(error)
                }
            }
          );
      });
  }
}

