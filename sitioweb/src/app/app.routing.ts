import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServiciosComponent } from './components/servicios/servicios.component';
import { HomeComponent } from './components/index/index.component';
import { BlogComponent } from './components/blog/blog.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { EducacionComponent } from './components/educacion/educacion.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { ProductosComponent } from './components/productos/productos.component';
import { UneteComponent } from './components/unete/unete.component';
import { NoticiablogComponent } from './components/noticiablog/noticiablog.component';


const APPROUTES: Routes = [
    { path: 'index', component: HomeComponent },
    { path: 'servicios', component: ServiciosComponent},
    { path: 'blog', component: BlogComponent},
    { path: 'blog/:idCategoriablog_Noticiablog', component: BlogComponent},
    { path: 'blog/:idNoticiablog/:idCategoriablog_Noticiablog/:tituloNoticiablog', component: NoticiablogComponent},
    { path: 'contacto', component: ContactoComponent},
    { path: 'educacion', component: EducacionComponent},
    { path: 'nosotros', component: NosotrosComponent},
    { path: 'productos', component: ProductosComponent},
    { path: 'unete', component: UneteComponent},
    { path: '**', component: HomeComponent }
];

export const appRoutingProviders: any[] = [];
export const ROUTING: ModuleWithProviders = RouterModule.forRoot(APPROUTES, { useHash: false });
