import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class ServiciosService{
	public url:string;
	public identity;
	public token;

	constructor(private _http:Http){
		this.url = GLOBAL.url;

	}

	getServicios(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'getServicios', {headers:headers})
		.pipe(map(res => res.json()));	
	}

	veServicio(idServicio:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'veServicio/'+idServicio, options)
		.pipe(map(res => res.json()));	
	}

}