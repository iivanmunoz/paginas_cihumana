import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class ProspectoService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	register(contacto){
		let json = JSON.stringify(contacto);
		let params = json;
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'registrarnuevoCliente', params, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	
	
}