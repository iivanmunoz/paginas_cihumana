import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class PuntosserviciosService{
	public url:string;
	public identity;
	public token;

	constructor(private _http:Http){
		this.url = GLOBAL.url;

	}

	getpuntoServicios(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'getpuntoServicios', {headers:headers})
		.pipe(map(res => res.json()));	
	}

	vepuntoServicios(idPuntoservicio:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'vepuntoServicios/'+idPuntoservicio, options)
		.pipe(map(res => res.json()));	
	}

	buscapuntoServicios(idServicio_Puntoservicio){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'buscapuntoServicios/'+idServicio_Puntoservicio, options)
		.pipe(map(res => res.json()));
	}
}