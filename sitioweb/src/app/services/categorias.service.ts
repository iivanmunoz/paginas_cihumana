import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class CategoriaService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	getCategorias(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'getCategoriaBlo', {headers:headers})
		.pipe(map(res => res.json()));	
	}

	getCategoria(idCategoriablog : string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'getCategoriaBlo/'+idCategoriablog, {headers:headers})
		.pipe(map(res => res.json()));	
	}
	
}