import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class PersonalService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;

	}

	getPersonal(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'obtenerPersonal', {headers:headers})
		.pipe(map(res => res.json()));	
	}


	vePersonal(idPersonal:string){
		let headers = new Headers({'Content-Type':'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'vePersonal/'+idPersonal, options)
		.pipe(map(res => res.json()));	
	}


}



