import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class BlogService{
	public url:string;

	constructor(private _http:Http){
		this.url = GLOBAL.url;
	}

	getNoticias(){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'obtenernoticiaBlog', {headers:headers})
		.pipe(map(res => res.json()));	
	}

	getNoticia(idCategoriablog_Noticiablog: string){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'obtenernoticiaBlog/'+idCategoriablog_Noticiablog, {headers:headers})
		.pipe(map(res => res.json()));	
	}

	getNoticiaBlog(idNoticiablog){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'obtenernoticiaidBlog/'+idNoticiablog, {headers:headers})
		.pipe(map(res => res.json()));	
	}
	
}




