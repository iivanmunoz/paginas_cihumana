import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { ConfigAPI } from '../models/configAPI';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class NoticiaService {
    private _ConfigAPI: ConfigAPI;
    constructor(private _Http: Http) {
        this._ConfigAPI = new ConfigAPI();
    }

    AddNoticia(noticia: FormData): Observable<any> {
        const headers = new Headers({ 'enctype': 'multipart/form-data' });
        return this._Http.post(`${this._ConfigAPI.URL}/noticia/AddNoticia/`, noticia,
            { headers: headers }).pipe(map(res => res.json()));
    }

    UpdateNoticia(idNoticia: number, formdata: FormData): Observable<any> {
        const headers = new Headers({ 'enctype': 'multipart/form-data' });
        return this._Http.put(`${ this._ConfigAPI.URL }/noticia/UpdateNoticia/${ idNoticia }`, formdata,
            { headers: headers }).pipe(map(res => res.json()));
    }

    ChangeStatusNoticia(idNoticia, status): Observable<any> {
        const json = JSON.stringify({ status });
        const params = json;
        const headers = new Headers({'Content-Type': 'application/json'});
        return this._Http.put(`${ this._ConfigAPI.URL }/noticia/ChangeStatusNoticia/${ idNoticia }`, params,
            { headers: headers }).pipe(map(res => res.json()));
    }

    GetAllNoticias(): Observable<any> {
        return this._Http.get(`${ this._ConfigAPI.URL }/noticia/GetAllNoticias`).pipe(map(res => res.json()));
    }

    GetNoticiasActivas(status: number): Observable<any> {
        return this._Http.get(`${ this._ConfigAPI.URL }/notica/GetNoticiasActivas/${ status }`)
            .pipe(map(res => res.json()));
    }

    GetNoticiasCategoria(idCategoria: number): Observable<any> {
        return this._Http.get(`${ this._ConfigAPI.URL }/notica/GetNoticiasCategoria/${ idCategoria }`)
            .pipe(map(res => res.json()));
    }

    DeleteNoticia(idNoticia: number): Observable<any> {
        return this._Http.get(`${ this._ConfigAPI.URL }/notica/DeleteNoticia/${ idNoticia }`)
            .pipe(map(res => res.json()));
    }
}
