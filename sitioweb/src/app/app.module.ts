import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CdkTableModule } from '@angular/cdk/table';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule
} from '@angular/material';

import { ROUTING, appRoutingProviders } from './app.routing';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/index/index.component';
import { ServiciosComponent} from './components/servicios/servicios.component';
import { MenuComponent} from './components/menu/menu.component';
import { BlogComponent} from './components/blog/blog.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { EducacionComponent} from './components/educacion/educacion.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { ProductosComponent } from './components/productos/productos.component';
import { UneteComponent } from './components/unete/unete.component';
import { FooterComponent } from './components/footer/footer.component';
import { EquipoComponent } from './components/equipo/equipo.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { DialogServicio } from './components/servicios/servicios.component';
import { DialogCliente } from './components/nosotros/nosotros.component';
import { DialogPersonal } from './components/nosotros/nosotros.component';
import { NoticiablogComponent } from './components/noticiablog/noticiablog.component';
import { DialogContacto } from './app.component';





@NgModule({
  exports: [
    CdkTableModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule
  ],
  declarations: [NoticiablogComponent]
})
export class MaterialModule { }


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    ServiciosComponent,
    BlogComponent,
    ContactoComponent,
    EducacionComponent,
    NosotrosComponent,
    ProductosComponent,
    UneteComponent,
    FooterComponent,
    EquipoComponent,
    ClientesComponent,
    AgendaComponent,
    DialogServicio,
    DialogCliente,
    DialogPersonal,
    DialogContacto
  ],
  entryComponents:[
  DialogServicio,
  DialogCliente,
  DialogPersonal,
  DialogContacto
  ],
  imports: [
    ROUTING,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
