/*
 Navicat Premium Data Transfer

 Source Server         : paginaedu_local
 Source Server Type    : MySQL
 Source Server Version : 50634
 Source Host           : localhost:3306
 Source Schema         : paginaedu

 Target Server Type    : MySQL
 Target Server Version : 50634
 File Encoding         : 65001

 Date: 18/11/2018 23:35:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrador
-- ----------------------------
DROP TABLE IF EXISTS `administrador`;
CREATE TABLE `administrador`  (
  `idAdministrador` int(11) NOT NULL AUTO_INCREMENT,
  `nombreAdministrador` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `apellidopaternoAdministrador` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `apellidomaternoAdministrador` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `correoAdministrador` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `passwordAdministrador` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fotoAdministrador` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `statusAdministrador` int(2) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idAdministrador`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of administrador
-- ----------------------------
INSERT INTO `administrador` VALUES (1, 'Erick', 'Medina', 'Peinado', 'erick@test', 'erick', '1-arcanine.jpg', 1);
INSERT INTO `administrador` VALUES (2, 'Test2', 'Test2', 'Test2', 'test2@test.com', 'test2', 'default.png', 1);
INSERT INTO `administrador` VALUES (3, 'Test4', 'Test4', 'Test4', 'Test4@test', 'Test4', 'default.png', 1);

-- ----------------------------
-- Table structure for aspirantes
-- ----------------------------
DROP TABLE IF EXISTS `aspirantes`;
CREATE TABLE `aspirantes`  (
  `idAspirante` int(11) NOT NULL AUTO_INCREMENT,
  `nombreAspirante` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `apellidopaternoAspirante` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `apellidomaternoAspirante` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `correoAspirante` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telefonoAspirante` bigint(20) NULL DEFAULT NULL,
  `cvAspirante` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ocupacionAspirante` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `statusAspirante` int(1) NULL DEFAULT 1,
  `fechaAspirante` date NULL DEFAULT NULL,
  PRIMARY KEY (`idAspirante`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of aspirantes
-- ----------------------------
INSERT INTO `aspirantes` VALUES (1, 'Test', 'Test', 'Test', 'test@test.com', 2147483647, '1-Indicador.docx', 'Test', 1, '2018-09-02');
INSERT INTO `aspirantes` VALUES (5, 'Gerardo', 'Zamudio', 'Gonzalez', 'gerardo@test.com', 2147483647, 'dsadsa.doc', 'Test2', 0, '2018-09-04');
INSERT INTO `aspirantes` VALUES (6, 'Angel', 'Romero', 'Velasco', 'angel@fake.com', 5511223344, 'asdsa.doc', 'Nini', 1, '2018-09-21');
INSERT INTO `aspirantes` VALUES (7, 'Angel Romero Velazco', '', '', 'bonne.chance2109@gmail.com', 5511223344, '7-Indicador.docx', 'Estudiante feik', 1, '2018-10-08');
INSERT INTO `aspirantes` VALUES (8, 'Testing', '', '', 'test@gmail.com', 5511223344, '8-Indicador.docx', 'asdasd', 1, '2018-10-08');
INSERT INTO `aspirantes` VALUES (9, 'Pepe PEcas', '', '', 'pepe@test.com', 5511223344, '9-Indicador.docx', 'asdasd', 1, '2018-11-08');
INSERT INTO `aspirantes` VALUES (10, 'Pepepe', '', '', 'lll@test.com', 5511223344, '10-Indicador.docx', 'sadasdsa', 1, '2018-11-08');
INSERT INTO `aspirantes` VALUES (11, 'asdasd', '', '', 'bas@test.com', 5511223344, '11-Indicador.docx', 'asdsa', 1, '2018-11-08');
INSERT INTO `aspirantes` VALUES (12, 'Final', '', '', 'final@test.com', 5511223344, '12-Indicador.docx', 'finaltest', 1, '2018-11-08');

-- ----------------------------
-- Table structure for categoriablog
-- ----------------------------
DROP TABLE IF EXISTS `categoriablog`;
CREATE TABLE `categoriablog`  (
  `idCategoriablog` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCategoriablog` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `statusCategoriablog` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idCategoriablog`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categoriablog
-- ----------------------------
INSERT INTO `categoriablog` VALUES (1, 'Negocios', 1);
INSERT INTO `categoriablog` VALUES (2, 'Estrategia', 1);
INSERT INTO `categoriablog` VALUES (3, 'Proyectos', 1);
INSERT INTO `categoriablog` VALUES (4, 'Aplicaciones', 1);
INSERT INTO `categoriablog` VALUES (5, 'Servicios Administrados', 1);
INSERT INTO `categoriablog` VALUES (6, 'Marketing', 1);
INSERT INTO `categoriablog` VALUES (7, 'Tecnologia', 1);

-- ----------------------------
-- Table structure for categoriaservicio
-- ----------------------------
DROP TABLE IF EXISTS `categoriaservicio`;
CREATE TABLE `categoriaservicio`  (
  `idCategoriaservicio_Servicio` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCategoriaservicio` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `statusCategoriaservicio` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idCategoriaservicio_Servicio`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categoriaservicio
-- ----------------------------
INSERT INTO `categoriaservicio` VALUES (1, 'Operativo', 1);
INSERT INTO `categoriaservicio` VALUES (2, 'Estratégico', 1);
INSERT INTO `categoriaservicio` VALUES (3, 'Táctico', 1);

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente`  (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `empresaCliente` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telefonoCliente` bigint(11) NULL DEFAULT NULL,
  `statusCliente` int(11) NULL DEFAULT 1,
  `fotoCliente` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idCliente`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cliente
-- ----------------------------
INSERT INTO `cliente` VALUES (1, 'Kemper', 2147483647, 1, '1-Kemper_Foundation_c.png');
INSERT INTO `cliente` VALUES (2, 'Casas GEO', 5511223344, 1, '2-casas geo.png');
INSERT INTO `cliente` VALUES (3, 'Scappino', 5511223344, 1, '3-36577_logoscappino.png');
INSERT INTO `cliente` VALUES (4, 'Policia Federal', 551122334, 1, '4-DVAExuZVAAAcol_.png');
INSERT INTO `cliente` VALUES (5, 'Hotel Group', 5522113344, 1, '5-logo-nhgroup_18.png');
INSERT INTO `cliente` VALUES (6, 'Fondo de Cultura Económica', 5511223344, 1, '6-Logo-Editorial.png');
INSERT INTO `cliente` VALUES (7, 'Escato', 5511223344, 1, '7-side5.png');
INSERT INTO `cliente` VALUES (8, 'Transportes Marva S.A de C.V', 551122344, 1, '8-transportes Marva.png');
INSERT INTO `cliente` VALUES (9, 'PartnerTaste', NULL, 1, '9-logo partner taste.png');

-- ----------------------------
-- Table structure for clienteservico
-- ----------------------------
DROP TABLE IF EXISTS `clienteservico`;
CREATE TABLE `clienteservico`  (
  `idClienteServicio` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente_ClienteServicio` int(11) NULL DEFAULT NULL,
  `idServicio_ClienteServicio` int(11) NULL DEFAULT NULL,
  `descripcionServicio_ClienteServicio` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fotoServicio_ClienteServicio` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idClienteServicio`) USING BTREE,
  INDEX `idCliente_ClienteServicio`(`idCliente_ClienteServicio`) USING BTREE,
  INDEX `idServicio_ClienteServicio`(`idServicio_ClienteServicio`) USING BTREE,
  CONSTRAINT `idCliente_ClienteServicio` FOREIGN KEY (`idCliente_ClienteServicio`) REFERENCES `cliente` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idServicio_ClienteServicio` FOREIGN KEY (`idServicio_ClienteServicio`) REFERENCES `servicio` (`idServicio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of clienteservico
-- ----------------------------
INSERT INTO `clienteservico` VALUES (1, 1, 3, 'Generación oportuna de estrategias de mercado utilizando inteligencia de negocios. Beneficio adicional: contribución al paperless.', 'EGD.png');
INSERT INTO `clienteservico` VALUES (2, 2, 3, 'Disminución de costos através del aumento de capacidades organizadas.', 'EGD.png');
INSERT INTO `clienteservico` VALUES (3, 3, 3, 'Oportunidad en la toma de decisiones operativas.', 'EGD.png');
INSERT INTO `clienteservico` VALUES (4, 4, 2, 'Mejora en los procesos de la mesa de servicio TI', 'PMO.png');
INSERT INTO `clienteservico` VALUES (5, 5, 3, 'Mejoras operativas através de la implantación de la cadena de valor.', 'EGD.png');
INSERT INTO `clienteservico` VALUES (6, 6, 3, 'Oportunidad en la toma de decisiones tácticas.', 'EGD.png');
INSERT INTO `clienteservico` VALUES (7, 7, 5, 'Mejoras operativas através de la implantación de la cadena de valor', 'BPO.png');
INSERT INTO `clienteservico` VALUES (8, 8, 1, 'Aumento en la generación de valor en la ejecución de estrategias en la cadena de valor.', 'DAE.png');
INSERT INTO `clienteservico` VALUES (9, 9, 3, 'Oportunidad en la toma de decisiones operativas.', 'EGD.png');
INSERT INTO `clienteservico` VALUES (10, 4, 5, '', 'BPO.png');
INSERT INTO `clienteservico` VALUES (11, 8, 3, NULL, 'EGD.png');

-- ----------------------------
-- Table structure for curso
-- ----------------------------
DROP TABLE IF EXISTS `curso`;
CREATE TABLE `curso`  (
  `idCurso` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCurso` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `descripcionCurso` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `aprendizajeCurso` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `objetivoCurso` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `duracionCurso` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `urlCurso` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `statusCurso` int(2) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idCurso`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of curso
-- ----------------------------
INSERT INTO `curso` VALUES (1, 'Planeación Estratégica', 'Define de manera correcta las estrategias que hagan crecer tu negocio.', 'Aprenderás sobre los fundamentos de la planeación estratégica, el proceso de planeación estratégica, evaluación del entorno, alternativas estratégicas.', 'Define de manera correcta las estrategias que hagan crecer tu negocio y conoce las estrategias que han llevado a las empresas a crecer y mantener un comportamiento saludable.', '18', 'https://www.dropbox.com/s/t06pcu5qs9gb0q6/Planeacion%20Estrategica%20.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (2, 'Administración de proyectos', 'Conoce de manera practica la forma de alinear, planear, organizar, dirigir y controlar\r\nConoce de manera practica la forma de alinear, planear, organizar, dirigir y controlar un proyecto.', 'Aprenderás sobre los fundamentos de un proyecto, estructuras de organización, equipos de trabajo, manejo de conflictos, liderazgo.', 'Conocer de manera practica la forma de alinear, planear, organizar, dirigir y controlar un proyecto utilizando las buenas practicas de la industria.', '18', 'https://www.dropbox.com/s/l5yc1rm2use7k0l/Administracion%20de%20Proyectos.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (3, 'Administración de riesgos', 'Conocer como gestionar adecuadamente cualquier riesgo y sus impactos derivados.', 'Aprenderás los fundamentos de la administración de riesgos, monitoreo y control de la alineación hacia la estrategia, tendencias y practicas en la administración de proyectos.', 'Conocer como gestionar adecuadamente cualquier riesgo y sus impactos derivados, manejar eficientemente cualquier siniestro y optimizar el balance de inversiones entre control y financiamiento de riesgos.', '18', 'https://www.dropbox.com/s/md0fh28pdh2zus0/Administraci%C3%B3n%20de%20Riesgos.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (4, 'Un enfoque practico Scrum', 'Generar rápidos resultados en tus proyectos bajo un modelo flexible y ágil a través de la practica de un experto.', 'Aprenderas el modelo de scrum, implementacion y adaptacion del modelo, fases, roles y principios de scrum.', 'Genera rápidos resultados en tus proyectos bajo un modelo flexible y ágil a través de la practica de un experto.', '18', 'https://www.dropbox.com/s/8x8r8189ki6d4z8/Un%20enfoque%20practico%20de%20Scrum.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (5, 'Administración de Proceso de Negocio', 'Aprender los conceptos generales de una arquitectura de procesos de negocio.', 'Aprenderas la metodología de proyectos IBM, notación BPMN2.0, introducción a la arquitectura institucional y el estándar de modelos de procesos de negocio BPMN.', 'Aprender los conceptos generales de una arquitectura de procesos de negocio a través de la notación estándar BPNM así como planear y ejecutar proyecto de BPM.', '18', 'https://www.dropbox.com/s/k9qlveks4ykqbkj/Administracion%20de%20Procesos%20de%20Negocio.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (6, 'Taller de Kotlin', 'Diseña aplicaciones android desde cero.', 'Aprenderás a desarrollar aplicaciones android con kotlin, entender las bases y la lógica de programación y ademas a entender la programación orientada a objetos.', 'El curso está diseñado para que principiantes que desean aprender a desarrollar aplicaciones android desde cero, ya que veras los fundamentos de programación que se necesitan para programar con este lenguaje.', '18', 'https://www.dropbox.com/s/5gtmsy278zwk7vm/Desarrollo%20de%20Aplicaciones%20Moviles%20Kotlin.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (7, 'Comportamiento del consumidor', 'Comprender el proceso de compra del consumidor a traves del analisis de su proceso de toma de desiciones.', 'Aprenderás la utilización eficaz de herramientas para la identificación de comportamientos, introducción al estudio del comportamiento del consumidor, proceso de decisión de compra del consumido.', 'Comprenderás el proceso de compra del consumidor a través del análisis de su proceso de toma de decisiones a fin de desarrollar las mejores tácticas de mercado.', '18', 'https://www.dropbox.com/s/i25tvakso9xczh7/Comportamiento%20del%20consumidor.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (8, 'Fidelización de Clientes', 'Conocer las estrategias mas efectivas y adecuadas para la fidelizacion del cliente.', 'Aprenderá sobre el servicio postventa y la importación de la fidelización del cliente, fidelización del cliente a través de una conciencia de marketing, herramientas de una fidelización eficaz.', 'Conocer las estrategias más efectivas y adecuadas para la fidelización del cliente.', '18', 'https://www.dropbox.com/s/ftn4gk22vp307ig/Fidelizacion%20del%20Cliente.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (9, 'Un enfoque practico del PMI', 'Conocer la gestión de proyectos de acuerdo a los lineamientos y buenas practicas del PMI.', 'Aprenderás sobre el marco conceptual, los procesos en la administración de proyectos, las áreas de conocimiento del PMI, conducta profesional u el método PMI en la practica.', 'Conocer la gestión de proyectos de acuerdo a los lineamientos y buenas practicas del PMI, conociendo los beneficios y bondades en el uso de la practica y prepararse en la certificación para PMP.', '18', 'https://www.dropbox.com/s/fuh7bksinpucsq3/Un%20Enfoque%20practico%20del%20PMI.pdf?dl=0', 1);
INSERT INTO `curso` VALUES (10, 'Alineación de proyectos a la estrategia de negocios', 'Conocer como evaluar y determinar la alineación hacia la estrategia.', 'Aprenderás sobre el mapa estratégico del negocio, garantizar la generación de valor, evaluación de los proyectos hacia la estrategia, monitoreo y control de alineación de proyectos.', 'Conocer como evaluar y determinar la alineación hacia la estrategia durante las fases de definición, planeación, ejecución, control y cierre de un proyecto.', '18', 'https://www.dropbox.com/s/i3f16hn0vnzcpyp/Alineacion%20de%20proyectos%20a%20la%20estrategia%20de%20negocio.pdf?dl=0', 1);

-- ----------------------------
-- Table structure for noticiablog
-- ----------------------------
DROP TABLE IF EXISTS `noticiablog`;
CREATE TABLE `noticiablog`  (
  `idNoticiablog` int(11) NOT NULL AUTO_INCREMENT,
  `idCategoriablog_Noticiablog` int(11) NULL DEFAULT NULL,
  `imagenNoticiablog` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tituloNoticiablog` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `descripcionNoticiablog` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fechaNoticiablog` date NULL DEFAULT NULL,
  `statusNoticiablog` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`idNoticiablog`) USING BTREE,
  INDEX `idCategoriablog_Noticiablog`(`idCategoriablog_Noticiablog`) USING BTREE,
  CONSTRAINT `idCategoriablog_Noticiablog` FOREIGN KEY (`idCategoriablog_Noticiablog`) REFERENCES `categoriablog` (`idCategoriablog`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of noticiablog
-- ----------------------------
INSERT INTO `noticiablog` VALUES (1, 1, 'Abra.png', 'Test1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus ', '2018-11-08', '1');
INSERT INTO `noticiablog` VALUES (2, 2, 'SL.jpg', 'Test2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus ', '2018-11-09', '1');
INSERT INTO `noticiablog` VALUES (3, 2, 'SL.jpg', 'Test3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, corporis maxime. Corporis tempore temporibus eos unde quia repellat consectetur quae enim laborum iure quidem officiis cumque, officia nam quibusdam beatae.\r Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, accusamus ', '2018-11-13', '1');

-- ----------------------------
-- Table structure for personal
-- ----------------------------
DROP TABLE IF EXISTS `personal`;
CREATE TABLE `personal`  (
  `idPersonal` int(11) NOT NULL AUTO_INCREMENT,
  `nombrePersonal` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `apellidopaternoPersonal` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `apellidomaternoPersonal` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telefonoPersonal` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `puestoPersonal` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sueldoPersonal` float(6, 2) NULL DEFAULT NULL,
  `statusPersonal` int(11) NULL DEFAULT NULL,
  `frasePersonal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fotoPersonal` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idPersonal`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of personal
-- ----------------------------
INSERT INTO `personal` VALUES (1, 'Amellaly', 'Hernández', '', '5511223344', 'Desarrollador', 0.00, 1, 'Me gusta diseñar conceptos que den la satisfacción que la gente necesita no por hacer dinero o conseguir lo que quieren.', 'ame_002.jpg');
INSERT INTO `personal` VALUES (2, 'Gerardo', 'Zamudio', '', '5511223344', 'PMO, Diseñadora', 0.00, 1, 'Me gusta crear soluciones de usuario que hagan parecer que la experiencia es comunicarse con otro ser humano.', 'gera_002.jpg');
INSERT INTO `personal` VALUES (3, 'Angel', 'Romero', 'Velasco', '5511223344', 'Tonto', 0.00, 1, 'Estoy chavo alv', '1-arcanine.jpg');
INSERT INTO `personal` VALUES (4, 'Angel', 'Romero', 'Velasco', '5511223344', 'Tonto', 0.00, 1, 'Estoy chavo alv', '1-arcanine.jpg');

-- ----------------------------
-- Table structure for prospectocita
-- ----------------------------
DROP TABLE IF EXISTS `prospectocita`;
CREATE TABLE `prospectocita`  (
  `idProspectocita` int(11) NOT NULL AUTO_INCREMENT,
  `nombreProspectocita` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `correoProspectocita` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `companiaProspectocita` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telefonoProspectocita` bigint(11) NULL DEFAULT NULL,
  `fechaProspectocita` date NULL DEFAULT NULL,
  `comentariosProspectocita` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `statusProspectocita` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idProspectocita`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of prospectocita
-- ----------------------------
INSERT INTO `prospectocita` VALUES (1, 'Test', 'luisgonx2109@gmail.com', 'TestCompany', 2147483647, '2018-09-05', NULL, 1);
INSERT INTO `prospectocita` VALUES (8, 'Erick', 'murdered_117@hotmail.com', 'asdas', 2147483647, '2018-09-06', 'asdasd', 1);
INSERT INTO `prospectocita` VALUES (10, 'Test', 'test@gmail.com', 'TestCompany', 2147483647, '2018-09-00', 'Testing', 1);
INSERT INTO `prospectocita` VALUES (11, 'Angel', 'manthys21999@gmail.com', 'TestCompany', 2147483647, '2018-09-01', 'Testing Register', 1);
INSERT INTO `prospectocita` VALUES (13, 'TestingLel', 'bonne.chance2109@gmail.com', 'Testingcompany', 2147483647, '2018-10-04', 'asdadsa', 1);
INSERT INTO `prospectocita` VALUES (14, 'Erick', 'erick.medina2109@gmail.com', 'asdas', 2147483647, '2018-10-04', 'asdasdsa', 1);
INSERT INTO `prospectocita` VALUES (15, 'FinalTestingContacts', 'final@test.com', 'TEsgingfinal', 5511223344, '2018-11-08', 'asdasdsa', 1);
INSERT INTO `prospectocita` VALUES (16, 'FinalTestingContacts', 'final@test.com', 'TEsgingfinal', 5511223344, '2018-11-08', 'asdasdsa', 1);
INSERT INTO `prospectocita` VALUES (17, 'Final', 'final@final.com', 'final', 5511223344, '2018-11-08', 'asdasdas', 1);
INSERT INTO `prospectocita` VALUES (19, 'Test', 'test@test.com', 'test', 5511223344, '2018-11-18', 'asdasdas', 1);
INSERT INTO `prospectocita` VALUES (20, 'asdas', 'asdasd@asdaqasd', 'asdas', 0, '2018-11-18', 'asdas', 1);

-- ----------------------------
-- Table structure for puntoservicio
-- ----------------------------
DROP TABLE IF EXISTS `puntoservicio`;
CREATE TABLE `puntoservicio`  (
  `idPuntoservicio` int(11) NOT NULL AUTO_INCREMENT,
  `descripcionPuntoservicio` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `idServicio_Puntoservicio` int(11) NULL DEFAULT NULL,
  `statusPuntoservicio` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`idPuntoservicio`) USING BTREE,
  INDEX `idServicio_Puntoservicio`(`idServicio_Puntoservicio`) USING BTREE,
  CONSTRAINT `idServicio_Puntoservicio` FOREIGN KEY (`idServicio_Puntoservicio`) REFERENCES `servicio` (`idServicio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of puntoservicio
-- ----------------------------
INSERT INTO `puntoservicio` VALUES (1, 'Creación de nuevas formas de comunicación con el cliente.', 1, 1);
INSERT INTO `puntoservicio` VALUES (2, 'Otorgando movilidad a los servicios.', 1, 1);
INSERT INTO `puntoservicio` VALUES (3, 'Tranformando el trabajo complejo en simple.', 1, 1);
INSERT INTO `puntoservicio` VALUES (4, 'Incremento en la eficiencia y eficacia en la atención al cliente.', 1, 1);
INSERT INTO `puntoservicio` VALUES (5, 'Cambio cultural en el trabajar.', 5, 1);
INSERT INTO `puntoservicio` VALUES (6, 'Generación de valor.', 5, 1);
INSERT INTO `puntoservicio` VALUES (7, 'Alineación de las capacidades de la organización.', 5, 1);
INSERT INTO `puntoservicio` VALUES (8, 'Conocimiento organizacional administrable.', 5, 1);
INSERT INTO `puntoservicio` VALUES (9, 'Cambio cultural por gestión de estrategias.', 3, 1);
INSERT INTO `puntoservicio` VALUES (10, 'Implantación modelo de gestión en ejecución de la estrategia.', 3, 1);
INSERT INTO `puntoservicio` VALUES (11, 'Vinculación de la estrategia con la operación.', 3, 1);
INSERT INTO `puntoservicio` VALUES (12, 'Alineación estratégica de las capacidades de la organización.', 3, 1);
INSERT INTO `puntoservicio` VALUES (13, 'Evaluar la generación de valor del proyecto.', 2, 1);
INSERT INTO `puntoservicio` VALUES (14, 'Alinear proyectos a la estrategia.', 2, 1);
INSERT INTO `puntoservicio` VALUES (15, 'Cultura en la implementación de proyectos.', 2, 1);
INSERT INTO `puntoservicio` VALUES (16, 'Gestión de indicadores para el cumplir objetivos del proyecto.', 2, 1);
INSERT INTO `puntoservicio` VALUES (17, 'Eficiencia de costos de operación de TI.', 7, 1);
INSERT INTO `puntoservicio` VALUES (18, 'Garantia en el nivel de servicio acordado.', 7, 1);
INSERT INTO `puntoservicio` VALUES (19, 'Apoyo en procesos de soporte.', 7, 1);
INSERT INTO `puntoservicio` VALUES (20, 'Buenas practicas en el proveer los servicios de TI.', 7, 1);
INSERT INTO `puntoservicio` VALUES (21, 'Enseñanza basada en el cambio cultural.', 6, 1);
INSERT INTO `puntoservicio` VALUES (22, 'Garantia por el uso de modelos de enseñanza de alta calidad.', 6, 1);
INSERT INTO `puntoservicio` VALUES (23, 'Seguimiento para asegurar la efectividad de la enseñanza.', 6, 1);
INSERT INTO `puntoservicio` VALUES (24, 'Cursos, talleres y diplomados con certificados de validez oficial.', 6, 1);

-- ----------------------------
-- Table structure for servicio
-- ----------------------------
DROP TABLE IF EXISTS `servicio`;
CREATE TABLE `servicio`  (
  `idServicio` int(11) NOT NULL AUTO_INCREMENT,
  `idCategoriaservicio_Servicio` int(11) NULL DEFAULT NULL,
  `nombreServicio` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `descripcionServicio` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `urlvideoServicio` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `statusServicio` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idServicio`) USING BTREE,
  INDEX `idCategoriaservicio_Servicio`(`idCategoriaservicio_Servicio`) USING BTREE,
  CONSTRAINT `idCategoriaservicio_Servicio` FOREIGN KEY (`idCategoriaservicio_Servicio`) REFERENCES `categoriaservicio` (`idCategoriaservicio_Servicio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of servicio
-- ----------------------------
INSERT INTO `servicio` VALUES (1, 1, 'DAE', 'Descripcion de DAE :V', 'https://www.youtube.com/embed/LOj6xTH9xKo?rel=0', 1);
INSERT INTO `servicio` VALUES (2, 1, 'PMO', 'DEscripcion de PMO', 'https://www.youtube.com/embed/768h6mWqU_w?rel=0', 1);
INSERT INTO `servicio` VALUES (3, 2, 'EGD', 'Descripcion de EGD', 'https://www.youtube.com/embed/6SRfXTucGds?rel=0', 1);
INSERT INTO `servicio` VALUES (4, 2, 'MKD', 'Descripcion de MKD', 'https://www.youtube.com/embed/LOj6xTH9xKo?rel=0', 1);
INSERT INTO `servicio` VALUES (5, 2, 'BPO', 'Descripcion de BPO', 'https://www.youtube.com/embed/LOj6xTH9xKo?rel=0', 1);
INSERT INTO `servicio` VALUES (6, 2, 'EDU', 'Descripcion de EDU', 'https://www.youtube.com/embed/Hq0BBeyAm5I', 1);
INSERT INTO `servicio` VALUES (7, 3, 'SAD', 'Descripcion de SAD', 'https://www.youtube.com/embed/LOj6xTH9xKo?rel=0', 1);

SET FOREIGN_KEY_CHECKS = 1;
